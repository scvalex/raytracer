all: debug

check:
	cargo fmt --all
	cargo check

debug: check
	cargo build

test: debug
	cargo test --all

watch:
	cargo watch

release: check
	cargo build --release

clean:
	git clean -dxf

clippy:
	cargo clippy
