use crate::contains::*;
use crate::eid::*;
use crate::entity::*;
use crate::intersect::*;
use crate::material::*;
use crate::matrix::*;
use crate::normal::*;
use crate::parent::*;
use crate::ray::*;
use crate::shape::ShapeStore;
use crate::tuple::*;

#[derive(Clone, Debug)]
pub struct Group {
    eid: Eid,
    transform: Matrix4,
    material: Material,
    parent: Option<Eid>,
    pub members: Vec<Eid>,
}

shape_boilerplate!(Group);

impl HasContains for Group {
    fn contains(&self, eid: &Eid, w: &dyn ShapeStore) -> bool {
        self.eid == *eid
            || self.members.contains(eid)
            || self
                .members
                .iter()
                .any(|e| w.find_shape(e).unwrap().contains(eid, w))
    }
}

impl Group {
    pub fn new(w: &mut dyn EidAllocator) -> Self {
        Group {
            eid: w.new_eid(),
            transform: M4_ID,
            material: Material::default(),
            parent: None,
            members: vec![],
        }
    }

    pub fn add_child<E>(&mut self, child: &mut E)
    where
        E: Entity + HasParent,
    {
        self.members.push(child.eid().clone());
        child.set_parent(&self.eid);
    }
}

impl HasIntersect for Group {
    fn local_intersect(&self, r: &Ray, q: &dyn ShapeStore) -> Vec<Intersection> {
        let mut xs: Vec<Intersection> = self
            .members
            .iter()
            .filter_map(|eid| q.find_shape(eid).map(|shape| shape.intersect(&r, q)))
            .flatten()
            .collect();
        xs.sort();
        xs
    }
}

impl HasNormal for Group {
    fn local_normal_at(&self, _: &Point, _: &Intersection, _: &dyn ShapeStore) -> Vector {
        // This should never happen.
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::shape::Shape;
    use crate::transform::*;
    use crate::world::World;

    #[test]
    fn new_groups_are_empty() {
        let group = Group::new(&mut World::new());
        assert_eq!(group.transform, M4_ID);
        assert!(group.members.is_empty());
    }

    #[test]
    fn ray_intersects_empty_group() {
        let mut w = World::new();
        let group = Group::new(&mut w);
        let r = ray(ORIGIN, vector!(0, 0, 1));
        assert!(group.local_intersect(&r, &w).is_empty());
    }

    #[test]
    fn ray_intersects_non_empty_group() {
        let mut w = World::new();
        let mut g = Group::new(&mut w);
        let s1 = {
            let s = w.add_sphere();
            g.add_child(s);
            s.eid()
        };
        let s2 = {
            let s = w.add_sphere();
            s.set_transform(translation(0., 0., -3.));
            g.add_child(s);
            s.eid()
        };
        let _s3 = {
            let s = w.add_sphere();
            s.set_transform(translation(5., 0., 0.));
            g.add_child(s);
        };
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let g_eid = g.eid();
        w.add_shape(Shape::Group(g));
        let xs = w.find_shape(&g_eid).unwrap().local_intersect(&r, &w);
        assert_eq!(xs.len(), 4);
        assert_eq!(xs[0].object, s2);
        assert_eq!(xs[1].object, s2);
        assert_eq!(xs[2].object, s1);
        assert_eq!(xs[3].object, s1);
    }

    #[test]
    fn intersecting_transformed_group() {
        let mut w = World::new();
        let mut g = Group::new(&mut w);
        g.set_transform(scaling(2., 2., 2.));
        {
            let s = w.add_sphere();
            s.set_transform(translation(5., 0., 0.));
            g.add_child(s);
        }
        let r = ray(point!(10, 0, -10), vector!(0, 0, 1));
        let g_eid = g.eid();
        w.add_shape(Shape::Group(g));
        let xs = w.find_shape(&g_eid).unwrap().intersect(&r, &w);
        assert_eq!(xs.len(), 2);
    }
}
