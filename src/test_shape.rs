use crate::contains::*;
use crate::eid::{Eid, EidAllocator};
use crate::intersect::{HasIntersect, Intersection};
use crate::material::{material, Material};
use crate::matrix::{Matrix4, M4_ID};
use crate::normal::HasNormal;
use crate::ray::Ray;
use crate::shape::ShapeStore;
use crate::tuple::{Point, Vector};
use std::cell::RefCell;

#[derive(Debug)]
pub struct TestShape {
    eid: Eid,
    transform: Matrix4,
    material: Material,
    saved_ray: RefCell<Option<Ray>>,
    parent: Option<Eid>,
}

shape_boilerplate!(TestShape);

impl HasContains for TestShape {
    fn contains(&self, eid: &Eid, _: &dyn ShapeStore) -> bool {
        self.eid == *eid
    }
}

impl HasNormal for TestShape {
    fn local_normal_at(&self, p: &Point, _: &Intersection, _: &dyn ShapeStore) -> Vector {
        vector!(p.x, p.y, p.z)
    }
}

impl HasIntersect for TestShape {
    fn local_intersect(&self, r: &Ray, _: &dyn ShapeStore) -> Vec<Intersection> {
        self.saved_ray.replace(Some(*r));
        vec![]
    }
}

impl TestShape {
    pub fn new(w: &mut dyn EidAllocator) -> Self {
        TestShape {
            eid: w.new_eid(),
            transform: M4_ID,
            material: material(),
            parent: None,
            saved_ray: RefCell::new(None),
        }
    }

    pub fn saved_ray(&self) -> Option<Ray> {
        self.saved_ray.borrow().as_ref().map(|r| *r)
    }
}
