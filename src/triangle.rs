use crate::contains::*;
use crate::eid::*;
use crate::intersect::*;
use crate::material::*;
use crate::matrix::*;
use crate::normal::*;
use crate::ray::*;
use crate::shape::ShapeStore;
use crate::tuple::{Point, Vector};

#[derive(Debug)]
pub struct Triangle {
    eid: Eid,
    transform: Matrix4,
    material: Material,
    parent: Option<Eid>,
    pub p1: Point,
    pub p2: Point,
    pub p3: Point,
    pub e1: Vector,
    pub e2: Vector,
    pub normal: Vector,
    pub smooth: bool,
    pub n1: Option<Vector>,
    pub n2: Option<Vector>,
    pub n3: Option<Vector>,
}

shape_boilerplate!(Triangle);

impl HasContains for Triangle {
    fn contains(&self, eid: &Eid, _: &dyn ShapeStore) -> bool {
        self.eid == *eid
    }
}

impl Triangle {
    pub fn new(w: &mut dyn EidAllocator, p1: Point, p2: Point, p3: Point) -> Self {
        let e1 = p2 - p1;
        let e2 = p3 - p1;
        Triangle {
            eid: w.new_eid(),
            transform: M4_ID,
            material: Material::default(),
            parent: None,
            p1,
            p2,
            p3,
            e1,
            e2,
            normal: e2.cross(e1).normalize(),
            smooth: false,
            n1: None,
            n2: None,
            n3: None,
        }
    }

    pub fn smooth(
        w: &mut dyn EidAllocator,
        p1: Point,
        p2: Point,
        p3: Point,
        n1: Vector,
        n2: Vector,
        n3: Vector,
    ) -> Self {
        let e1 = p2 - p1;
        let e2 = p3 - p1;
        Triangle {
            eid: w.new_eid(),
            transform: M4_ID,
            material: Material::default(),
            parent: None,
            p1,
            p2,
            p3,
            e1,
            e2,
            normal: e2.cross(e1).normalize(),
            smooth: true,
            n1: Some(n1),
            n2: Some(n2),
            n3: Some(n3),
        }
    }
}

impl HasIntersect for Triangle {
    fn local_intersect(&self, r: &Ray, _: &dyn ShapeStore) -> Vec<Intersection> {
        let dir_cross_e2 = r.direction.cross(self.e2);
        let det = self.e1.dot(dir_cross_e2);
        if det.abs() < std::f64::EPSILON {
            vec![]
        } else {
            let f = 1. / det;
            let p1_to_origin = r.origin - self.p1;
            let u = f * p1_to_origin.dot(dir_cross_e2);
            if u < 0. || u > 1. {
                vec![]
            } else {
                let origin_cross_e1 = p1_to_origin.cross(self.e1);
                let v = f * r.direction.dot(origin_cross_e1);
                if v < 0. || (u + v) > 1. {
                    vec![]
                } else {
                    let t = f * self.e2.dot(origin_cross_e1);
                    vec![intersection_with_uv(t, self.eid, u, v)]
                }
            }
        }
    }
}

impl HasNormal for Triangle {
    fn local_normal_at(&self, _: &Point, hit: &Intersection, _: &dyn ShapeStore) -> Vector {
        if self.smooth {
            let u = hit.u.unwrap();
            let v = hit.v.unwrap();
            self.n2.unwrap() * u + self.n3.unwrap() * v + self.n1.unwrap() * (1. - u - v)
        } else {
            self.normal
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::robust_eq::*;
    use crate::shape::Shape;
    use crate::tuple::*;
    use crate::world::World;

    #[test]
    fn triangle_constructor() {
        let mut w = World::new();
        let p1 = point!(0, 1, 0);
        let p2 = point!(-1, 0, 0);
        let p3 = point!(1, 0, 0);
        let t = Triangle::new(&mut w, p1, p2, p3);
        assert_eq!(t.p1, p1);
        assert_eq!(t.p2, p2);
        assert_eq!(t.p3, p3);
        assert_eq!(t.e1, vector!(-1, -1, 0));
        assert_eq!(t.e2, vector!(1, -1, 0));
        assert_eq!(t.normal, vector!(0, 0, -1));
    }

    #[test]
    fn triangle_normal() {
        let mut w = World::new();
        let t = Triangle::new(&mut w, point!(0, 1, 0), point!(-1, 0, 0), point!(1, 0, 0));
        let n1 = t.local_normal_at(&point!(0, 0.5, 0), &intersection(0., t.eid), &w);
        let n2 = t.local_normal_at(&point!(-0.5, 0.75, 0), &intersection(0., t.eid), &w);
        let n3 = t.local_normal_at(&point!(0.5, -0.25, 0), &intersection(0., t.eid), &w);
        assert_eq!(n1, t.normal);
        assert_eq!(n2, t.normal);
        assert_eq!(n3, t.normal);
    }

    #[test]
    fn intersect_ray_parallel_with_triangle() {
        let mut w = World::new();
        let t = {
            let t = Triangle::new(&mut w, point!(0, 1, 0), point!(-1, 0, 0), point!(1, 0, 0));
            let t_eid = t.eid;
            w.add_shape(Shape::Triangle(t));
            t_eid
        };
        let r = ray(point!(0, -1, -2), vector!(0, 1, 0));
        let xs = w.find_shape(&t).unwrap().local_intersect(&r, &w);
        assert_eq!(xs.len(), 0);
    }

    #[test]
    fn ray_misses_p1_p3_edge() {
        let mut w = World::new();
        let t = {
            let t = Triangle::new(&mut w, point!(0, 1, 0), point!(-1, 0, 0), point!(1, 0, 0));
            let t_eid = t.eid;
            w.add_shape(Shape::Triangle(t));
            t_eid
        };
        let r = ray(point!(1, 1, -2), vector!(0, 0, 1));
        let xs = w.find_shape(&t).unwrap().local_intersect(&r, &w);
        assert_eq!(xs.len(), 0);
    }

    #[test]
    fn ray_misses_p1_p2_edge() {
        let mut w = World::new();
        let t = {
            let t = Triangle::new(&mut w, point!(0, 1, 0), point!(-1, 0, 0), point!(1, 0, 0));
            let t_eid = t.eid;
            w.add_shape(Shape::Triangle(t));
            t_eid
        };
        let r = ray(point!(-1, 1, -2), vector!(0, 0, 1));
        let xs = w.find_shape(&t).unwrap().local_intersect(&r, &w);
        assert_eq!(xs.len(), 0);
    }

    #[test]
    fn ray_misses_p2_p3_edge() {
        let mut w = World::new();
        let t = {
            let t = Triangle::new(&mut w, point!(0, 1, 0), point!(-1, 0, 0), point!(1, 0, 0));
            let t_eid = t.eid;
            w.add_shape(Shape::Triangle(t));
            t_eid
        };
        let r = ray(point!(0, -1, -2), vector!(0, 0, 1));
        let xs = w.find_shape(&t).unwrap().local_intersect(&r, &w);
        assert_eq!(xs.len(), 0);
    }

    #[test]
    fn ray_strikes_triangle() {
        let mut w = World::new();
        let t = {
            let t = Triangle::new(&mut w, point!(0, 1, 0), point!(-1, 0, 0), point!(1, 0, 0));
            let t_eid = t.eid;
            w.add_shape(Shape::Triangle(t));
            t_eid
        };
        let r = ray(point!(0, 0.5, -2), vector!(0, 0, 1));
        let xs = w.find_shape(&t).unwrap().local_intersect(&r, &w);
        assert_eq!(xs.len(), 1);
        assert_eq!(xs[0].t, 2.);
    }

    fn smooth_triangle(w: &mut dyn EidAllocator) -> Triangle {
        let p1 = point!(0, 1, 0);
        let p2 = point!(-1, 0, 0);
        let p3 = point!(1, 0, 0);
        let n1 = vector!(0, 1, 0);
        let n2 = vector!(-1, 0, 0);
        let n3 = vector!(1, 0, 0);
        Triangle::smooth(w, p1, p2, p3, n1, n2, n3)
    }

    #[test]
    fn construct_smooth_triangle() {
        let mut w = World::new();
        let _ = smooth_triangle(&mut w);
    }

    #[test]
    fn smooth_triangle_intersection_stores_uv() {
        let mut w = World::new();
        let tri = smooth_triangle(&mut w);
        let r = ray(point!(-0.2, 0.3, -2), vector!(0, 0, 1));
        let xs = tri.local_intersect(&r, &w);
        assert!(float_eq_robust(xs[0].u.unwrap(), 0.45));
        assert!(float_eq_robust(xs[0].v.unwrap(), 0.25));
    }

    #[test]
    fn smooth_triangle_normal() {
        let mut w = World::new();
        let tri = smooth_triangle(&mut w);
        let i = intersection_with_uv(1., tri.eid, 0.45, 0.25);
        let n = Shape::Triangle(tri).normal_at(&ORIGIN, &i, &w);
        assert_eq!(n, vector!(-0.5547, 0.83205, 0.));
    }

    #[test]
    fn preparing_normal_on_smooth_triangle() {
        let mut w = World::new();
        let tri = smooth_triangle(&mut w);
        let i = intersection_with_uv(1., tri.eid, 0.45, 0.25);
        let r = ray(point!(-0.2, 0.3, -2), vector!(0, 0, 1));
        w.add_shape(Shape::Triangle(tri));
        let comps = w.prepare_computations(&i, &r, &[i]);
        assert_eq!(comps.normalv, vector!(-0.5547, 0.83205, 0));
    }
}
