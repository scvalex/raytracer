use crate::robust_eq::*;
use crate::tuple::*;

#[derive(Clone, Copy, Debug)]
pub enum D11 {}

#[derive(Clone, Copy, Debug)]
pub enum D22 {}

#[derive(Clone, Copy, Debug)]
pub enum D33 {}

#[derive(Clone, Copy, Debug)]
pub enum D44 {}

pub trait Dim {
    const DIM: usize;
    type MatrixArray;
    type SmallerDim;
    fn new_matrix_array() -> Self::MatrixArray;
    fn get(s: &Self::MatrixArray, row: usize, col: usize) -> f64;
    fn get_mut(s: &mut Self::MatrixArray, row: usize, col: usize) -> &mut f64;
}

impl Dim for D22 {
    const DIM: usize = 2;
    type MatrixArray = [[f64; 2]; 2];
    type SmallerDim = D11;

    fn new_matrix_array() -> Self::MatrixArray {
        [[0.; 2]; 2]
    }

    fn get(s: &Self::MatrixArray, row: usize, col: usize) -> f64 {
        s[row][col]
    }

    fn get_mut(s: &mut Self::MatrixArray, row: usize, col: usize) -> &mut f64 {
        &mut s[row][col]
    }
}

impl Dim for D33 {
    const DIM: usize = 3;
    type MatrixArray = [[f64; 3]; 3];
    type SmallerDim = D22;

    fn new_matrix_array() -> Self::MatrixArray {
        [[0.; 3]; 3]
    }

    fn get(s: &Self::MatrixArray, row: usize, col: usize) -> f64 {
        s[row][col]
    }

    fn get_mut(s: &mut Self::MatrixArray, row: usize, col: usize) -> &mut f64 {
        &mut s[row][col]
    }
}

impl Dim for D44 {
    const DIM: usize = 4;
    type MatrixArray = [[f64; 4]; 4];
    type SmallerDim = D33;

    fn new_matrix_array() -> Self::MatrixArray {
        [[0.; 4]; 4]
    }

    fn get(s: &Self::MatrixArray, row: usize, col: usize) -> f64 {
        s[row][col]
    }

    fn get_mut(s: &mut Self::MatrixArray, row: usize, col: usize) -> &mut f64 {
        &mut s[row][col]
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Matrix<D: Dim>(D::MatrixArray);

pub type Matrix2 = Matrix<D22>;
pub type Matrix3 = Matrix<D33>;
pub type Matrix4 = Matrix<D44>;

pub static M4_ID: Matrix4 = Matrix([
    [1., 0., 0., 0.],
    [0., 1., 0., 0.],
    [0., 0., 1., 0.],
    [0., 0., 0., 1.],
]);

pub fn mat2(data: [[f64; 2]; 2]) -> Matrix<D22> {
    Matrix(data)
}

pub fn mat3(data: [[f64; 3]; 3]) -> Matrix<D33> {
    Matrix(data)
}

pub fn mat4(data: [[f64; 4]; 4]) -> Matrix<D44> {
    Matrix(data)
}

impl<D: Dim> Default for Matrix<D> {
    fn default() -> Self {
        Self::new()
    }
}

impl<D: Dim> Matrix<D> {
    pub fn new() -> Self {
        Matrix(D::new_matrix_array())
    }

    pub fn get(&self, row: usize, col: usize) -> f64 {
        assert!(row < D::DIM && col < D::DIM);
        <D as Dim>::get(&self.0, row, col)
    }

    pub fn get_mut(&mut self, row: usize, col: usize) -> &mut f64 {
        assert!(row < D::DIM && col < D::DIM);
        <D as Dim>::get_mut(&mut self.0, row, col)
    }

    pub fn transpose(&self) -> Matrix<D> {
        let mut res = Matrix::new();
        for i in 0..D::DIM {
            for j in 0..D::DIM {
                *res.get_mut(i, j) = self.get(j, i);
            }
        }
        res
    }
}

impl<D> Matrix<D>
where
    D: Dim,
    <D as Dim>::SmallerDim: Dim,
    Matrix<D::SmallerDim>: HasDeterminant,
{
    pub fn submatrix(&self, row: usize, col: usize) -> Matrix<D::SmallerDim> {
        let mut res = Matrix::new();
        let mut i_res = 0;
        for i in 0..D::DIM {
            if i != row {
                let mut j_res = 0;
                for j in 0..D::DIM {
                    if j != col {
                        *res.get_mut(i_res, j_res) = self.get(i, j);
                        j_res += 1;
                    }
                }
                i_res += 1;
            }
        }
        res
    }

    pub fn minor(&self, row: usize, col: usize) -> f64 {
        self.submatrix(row, col).determinant()
    }

    pub fn cofactor(&self, row: usize, col: usize) -> f64 {
        let minor = self.minor(row, col);
        if (row + col) % 2 == 0 {
            minor
        } else {
            -minor
        }
    }
}

pub trait HasDeterminant {
    fn determinant(&self) -> f64;
}

impl HasDeterminant for Matrix<D22> {
    fn determinant(&self) -> f64 {
        self.get(0, 0) * self.get(1, 1) - self.get(0, 1) * self.get(1, 0)
    }
}

fn determinant_gen<D>(mat: &Matrix<D>) -> f64
where
    D: Dim,
    <D as Dim>::SmallerDim: Dim,
    Matrix<D::SmallerDim>: HasDeterminant,
{
    let mut determinant = 0.0;
    for i in 0..D::DIM {
        determinant += mat.get(0, i) * mat.cofactor(0, i);
    }
    determinant
}

impl HasDeterminant for Matrix<D33> {
    fn determinant(&self) -> f64 {
        determinant_gen::<D33>(&self)
    }
}

impl HasDeterminant for Matrix<D44> {
    fn determinant(&self) -> f64 {
        determinant_gen::<D44>(&self)
    }
}

impl<D> Matrix<D>
where
    D: Dim,
    Matrix<D>: HasDeterminant,
    <D as Dim>::SmallerDim: Dim,
    Matrix<D::SmallerDim>: HasDeterminant,
{
    pub fn is_invertible(&self) -> bool {
        !float_eq_robust(self.determinant(), 0.0)
    }

    pub fn inverse(&self) -> Matrix<D> {
        assert!(self.is_invertible());
        let mut res = Matrix::new();
        let determinant = self.determinant();
        for col in 0..D::DIM {
            for row in 0..D::DIM {
                *res.get_mut(col, row) = self.cofactor(row, col) / determinant;
            }
        }
        res
    }
}

impl<D: Dim> std::ops::Mul<Matrix<D>> for Matrix<D> {
    type Output = Matrix<D>;

    fn mul(self, other: Matrix<D>) -> Self::Output {
        let mut res = Matrix::new();
        for i in 0..D::DIM {
            for j in 0..D::DIM {
                for k in 0..D::DIM {
                    *res.get_mut(i, j) += self.get(i, k) * other.get(k, j);
                }
            }
        }
        res
    }
}

impl std::ops::Mul<Tuple> for Matrix<D44> {
    type Output = Tuple;

    fn mul(self, other: Tuple) -> Tuple {
        tuple!(
            self.get(0, 0) * other.x
                + self.get(0, 1) * other.y
                + self.get(0, 2) * other.z
                + self.get(0, 3) * other.w,
            self.get(1, 0) * other.x
                + self.get(1, 1) * other.y
                + self.get(1, 2) * other.z
                + self.get(1, 3) * other.w,
            self.get(2, 0) * other.x
                + self.get(2, 1) * other.y
                + self.get(2, 2) * other.z
                + self.get(2, 3) * other.w,
            self.get(3, 0) * other.x
                + self.get(3, 1) * other.y
                + self.get(3, 2) * other.z
                + self.get(3, 3) * other.w
        )
    }
}

impl<D: Dim> PartialEq for Matrix<D> {
    fn eq(&self, other: &Matrix<D>) -> bool {
        for i in 0..D::DIM {
            for j in 0..D::DIM {
                if !float_eq_robust(self.get(i, j), other.get(i, j)) {
                    return false;
                }
            }
        }
        true
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn matrix4x4_tests() {
        // bullet dodge!
        let m = mat4([
            [1.0, 2.0, 3.0, 4.0],
            [5.5, 6.5, 7.5, 8.5],
            [9.0, 10.0, 11.0, 12.0],
            [13.5, 14.5, 15.5, 16.5],
        ]);
        assert_eq!(m.get(0, 0), 1.0);
        assert_eq!(m.get(0, 3), 4.0);
        assert_eq!(m.get(1, 0), 5.5);
        assert_eq!(m.get(1, 2), 7.5);
        assert_eq!(m.get(2, 2), 11.0);
        assert_eq!(m.get(3, 0), 13.5);
        assert_eq!(m.get(3, 2), 15.5);
    }

    #[test]
    fn matrix2x2_tests() {
        let m = mat2([
            [-3.0, 5.0], //
            [1.0, -2.0],
        ]);
        assert_eq!(m.get(0, 0), -3.0);
        assert_eq!(m.get(0, 1), 5.0);
        assert_eq!(m.get(1, 0), 1.0);
        assert_eq!(m.get(1, 1), -2.0);
    }

    #[test]
    fn matrix3x3_tests() {
        let m = mat3([
            [-3.0, 5.0, 0.0],  //
            [1.0, -2.0, -7.0], //
            [0.0, 1.0, 1.0],
        ]);
        assert_eq!(m.get(0, 0), -3.0);
        assert_eq!(m.get(1, 1), -2.0);
        assert_eq!(m.get(2, 2), 1.0);
    }

    #[test]
    fn matrix_eq_tests() {
        assert_eq!(
            mat4([
                [1.0, 2.0, 3.0, 4.0],
                [5.0, 6.0, 7.0, 8.0],
                [9.0, 8.0, 7.0, 6.0],
                [5.0, 4.0, 3.0, 2.0]
            ]),
            mat4([
                [1.0, 2.0, 3.0, 4.0],
                [5.0, 6.0, 7.0, 8.0],
                [9.0, 8.0, 7.0, 6.0],
                [5.0, 4.0, 3.0, 2.0]
            ])
        );
        assert_ne!(
            mat4([
                [1.0, 2.0, 3.0, 4.0],
                [5.0, 6.0, 7.0, 8.0],
                [9.0, 8.0, 7.0, 6.0],
                [5.0, 4.0, 3.0, 2.0]
            ]),
            mat4([
                [2.0, 3.0, 4.0, 5.0],
                [6.0, 7.0, 8.0, 9.0],
                [8.0, 7.0, 6.0, 5.0],
                [4.0, 3.0, 2.0, 1.0]
            ])
        );
    }

    #[test]
    fn test_mul() {
        let a = mat4([
            [1.0, 2.0, 3.0, 4.0],
            [5.0, 6.0, 7.0, 8.0],
            [9.0, 8.0, 7.0, 6.0],
            [5.0, 4.0, 3.0, 2.0],
        ]);
        let b = mat4([
            [-2.0, 1.0, 2.0, 3.0],
            [3.0, 2.0, 1.0, -1.0],
            [4.0, 3.0, 6.0, 5.0],
            [1.0, 2.0, 7.0, 8.0],
        ]);
        assert_eq!(
            mat4([
                [20.0, 22.0, 50.0, 48.0],
                [44.0, 54.0, 114.0, 108.0],
                [40.0, 58.0, 110.0, 102.0],
                [16.0, 26.0, 46.0, 42.0]
            ]),
            a * b
        );
    }

    #[test]
    fn test_mul_with_tuple() {
        assert_eq!(
            tuple!(18, 24, 33, 1),
            mat4([
                [1.0, 2.0, 3.0, 4.0],
                [2.0, 4.0, 4.0, 2.0],
                [8.0, 6.0, 4.0, 1.0],
                [0.0, 0.0, 0.0, 1.0]
            ]) * tuple!(1, 2, 3, 1)
        );
    }

    #[test]
    fn test_mul_with_identity() {
        let a = mat4([
            [0.0, 1.0, 2.0, 4.0],
            [1.0, 2.0, 4.0, 8.0],
            [2.0, 4.0, 8.0, 16.0],
            [4.0, 8.0, 16.0, 32.0],
        ]);
        let res = a * M4_ID;
        assert_eq!(res, a);
    }

    #[test]
    fn test_transpose() {
        assert_eq!(
            mat4([
                [0.0, 9.0, 3.0, 0.0],
                [9.0, 8.0, 0.0, 8.0],
                [1.0, 8.0, 5.0, 3.0],
                [0.0, 0.0, 5.0, 8.0]
            ])
            .transpose(),
            mat4([
                [0.0, 9.0, 1.0, 0.0],
                [9.0, 8.0, 8.0, 0.0],
                [3.0, 0.0, 5.0, 5.0],
                [0.0, 8.0, 3.0, 8.0]
            ])
        )
    }

    #[test]
    fn test_transpose_id() {
        assert_eq!(M4_ID.transpose(), M4_ID)
    }

    #[test]
    fn test_determinant() {
        assert_eq!(
            mat2([
                [1.0, 5.0], //
                [-3.0, 2.0]
            ])
            .determinant(),
            17.0
        )
    }

    #[test]
    fn test_submatrix3() {
        assert_eq!(
            mat3([
                [1.0, 5.0, 0.0],  //
                [-3.0, 2.0, 7.0], //
                [0.0, 6.0, -3.0]
            ])
            .submatrix(0, 2),
            mat2([
                [-3.0, 2.0], //
                [0.0, 6.0]
            ])
        )
    }

    #[test]
    fn test_submatrix4() {
        assert_eq!(
            mat4([
                [-6.0, 1.0, 1.0, 6.0],
                [-8.0, 5.0, 8.0, 6.0],
                [-1.0, 0.0, 8.0, 2.0],
                [-7.0, 1.0, -1.0, 1.0]
            ])
            .submatrix(2, 1),
            mat3([
                [-6.0, 1.0, 6.0], //
                [-8.0, 8.0, 6.0], //
                [-7.0, -1.0, 1.0]
            ])
        )
    }

    #[test]
    fn test_minor() {
        assert_eq!(
            mat3([
                [3.0, 5.0, 0.0],   //
                [2.0, -1.0, -7.0], //
                [6.0, -1.0, 5.0]
            ])
            .minor(1, 0),
            25.0
        )
    }

    #[test]
    fn test_cofactor() {
        let a = mat3([
            [3.0, 5.0, 0.0],   //
            [2.0, -1.0, -7.0], //
            [6.0, -1.0, 5.0],
        ]);
        assert_eq!(a.cofactor(0, 0), -12.0);
        assert_eq!(a.cofactor(1, 0), -25.0);
    }

    #[test]
    fn test_determinant3() {
        assert_eq!(
            mat3([
                [1.0, 2.0, 6.0],   //
                [-5.0, 8.0, -4.0], //
                [2.0, 6.0, 4.0]
            ])
            .determinant(),
            -196.0
        )
    }

    #[test]
    fn test_determinant4() {
        assert_eq!(
            mat4([
                [-2.0, -8.0, 3.0, 5.0],
                [-3.0, 1.0, 7.0, 3.0],
                [1.0, 2.0, -9.0, 6.0],
                [-6.0, 7.0, 7.0, -9.0]
            ])
            .determinant(),
            -4071.0
        )
    }

    #[test]
    fn test_is_invertible() {
        assert_eq!(
            mat4([
                [6.0, 4.0, 4.0, 4.0],
                [5.0, 5.0, 7.0, 6.0],
                [4.0, -9.0, 3.0, -7.0],
                [9.0, 1.0, 7.0, -6.0]
            ])
            .is_invertible(),
            true
        );
        assert_eq!(
            mat4([
                [-4.0, 2.0, -2.0, -3.0],
                [9.0, 6.0, 2.0, 6.0],
                [0.0, -5.0, 1.0, -5.0],
                [0.0, 0.0, 0.0, 0.0]
            ])
            .is_invertible(),
            false
        )
    }

    #[test]
    fn test_inverse1() {
        assert_eq!(
            mat4([
                [-5.0, 2.0, 6.0, -8.0],
                [1.0, -5.0, 1.0, 8.0],
                [7.0, 7.0, -6.0, -7.0],
                [1.0, -3.0, 7.0, 4.0]
            ])
            .inverse(),
            mat4([
                [0.21805, 0.45113, 0.24060, -0.04511],
                [-0.80827, -1.45677, -0.44361, 0.52068],
                [-0.07895, -0.22368, -0.05263, 0.19737],
                [-0.52256, -0.81391, -0.30075, 0.30639]
            ])
        )
    }

    #[test]
    fn test_inverse2() {
        assert_eq!(
            mat4([
                [8.0, -5.0, 9.0, 2.0],
                [7.0, 5.0, 6.0, 1.0],
                [-6.0, 0.0, 9.0, 6.0],
                [-3.0, 0.0, -9.0, -4.0]
            ])
            .inverse(),
            mat4([
                [-0.15385, -0.15385, -0.28205, -0.53846],
                [-0.07692, 0.12308, 0.02564, 0.03077],
                [0.35897, 0.35897, 0.43590, 0.92308],
                [-0.69231, -0.69231, -0.76923, -1.92308]
            ])
        )
    }

    #[test]
    fn test_inverse3() {
        assert_eq!(
            mat4([
                [9.0, 3.0, 0.0, 9.0],
                [-5.0, -2.0, -6.0, -3.0],
                [-4.0, 9.0, 6.0, 4.0],
                [-7.0, 6.0, 6.0, 2.0]
            ])
            .inverse(),
            mat4([
                [-0.04074, -0.07778, 0.14444, -0.22222],
                [-0.07778, 0.03333, 0.36667, -0.33333],
                [-0.02901, -0.14630, -0.10926, 0.12963],
                [0.17778, 0.06667, -0.26667, 0.33333]
            ])
        )
    }

    #[test]
    fn test_inverse_id() {
        let a = mat4([
            [3.0, -9.0, 7.0, 3.0],
            [3.0, -8.0, 2.0, -9.0],
            [-4.0, 4.0, 4.0, 1.0],
            [-6.0, 5.0, -1.0, 1.0],
        ]);
        let b = mat4([
            [8.0, 2.0, 2.0, 2.0],
            [3.0, -1.0, 7.0, 0.0],
            [7.0, 0.0, 5.0, 4.0],
            [6.0, -2.0, 0.0, 5.0],
        ]);
        let c = a * b;
        assert_eq!(c * b.inverse(), a)
    }

    #[test]
    fn test_inverse_identity() {
        assert_eq!(M4_ID.inverse(), M4_ID)
    }

    #[test]
    fn test_inverse_mult_self() {
        let a = mat4([
            [3.0, -9.0, 7.0, 3.0],
            [3.0, -8.0, 2.0, -9.0],
            [-4.0, 4.0, 4.0, 1.0],
            [-6.0, 5.0, -1.0, 1.0],
        ]);
        assert_eq!(a * a.inverse(), M4_ID);
        assert_eq!(a.inverse() * a, M4_ID);
    }
}
