use crate::contains::*;
use crate::eid::*;
use crate::intersect::*;
use crate::material::*;
use crate::matrix::*;
use crate::normal::*;
use crate::ray::*;
use crate::robust_eq::*;
use crate::shape::ShapeStore;
use crate::tuple::*;

#[derive(Clone, Debug)]
pub struct Plane {
    eid: Eid,
    transform: Matrix4,
    material: Material,
    parent: Option<Eid>,
}

shape_boilerplate!(Plane);

impl HasContains for Plane {
    fn contains(&self, eid: &Eid, _: &dyn ShapeStore) -> bool {
        self.eid == *eid
    }
}

impl Plane {
    pub fn new(world: &mut dyn EidAllocator) -> Self {
        Plane {
            eid: world.new_eid(),
            transform: M4_ID,
            material: material(),
            parent: None,
        }
    }
}

impl HasNormal for Plane {
    fn local_normal_at(&self, _: &Point, _: &Intersection, _: &dyn ShapeStore) -> Vector {
        vector!(0, 1, 0)
    }
}

impl HasIntersect for Plane {
    fn local_intersect(&self, r: &Ray, _: &dyn ShapeStore) -> Vec<Intersection> {
        if float_eq_robust(r.direction.y, 0.) {
            vec![]
        } else {
            let t = -r.origin.y / r.direction.y;
            vec![intersection(t, self.eid)]
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::entity::Entity;
    use crate::world::World;

    #[test]
    fn normal_same_everywhere() {
        let mut w = World::new();
        let p = Plane::new(&mut w);
        let n1 = p.local_normal_at(&point!(0, 0, 0), &intersection(0., p.eid), &w);
        let n2 = p.local_normal_at(&point!(10, 0, -10), &intersection(0., p.eid), &w);
        let n3 = p.local_normal_at(&point!(-5, 0, 150), &intersection(0., p.eid), &w);
        assert_eq!(n1, vector!(0, 1, 0));
        assert_eq!(n2, vector!(0, 1, 0));
        assert_eq!(n3, vector!(0, 1, 0));
    }

    #[test]
    fn intersect_ray_parallel() {
        let mut w = World::new();
        let p = Plane::new(&mut w);
        let r = ray(point!(0, 10, 0), vector!(0, 0, 1));
        assert!(p.local_intersect(&r, &w).is_empty());
    }

    #[test]
    fn intersect_ray_inside() {
        let mut w = World::new();
        let p = Plane::new(&mut w);
        let r = ray(point!(0, 0, 0), vector!(0, 0, 1));
        assert!(p.local_intersect(&r, &w).is_empty());
    }

    #[test]
    fn intersect_ray_from_above() {
        let mut w = World::new();
        let p = Plane::new(&mut w);
        let r = ray(point!(0, 1, 0), vector!(0, -1, 0));
        let xs = p.local_intersect(&r, &w);
        assert_eq!(xs.len(), 1);
        assert_eq!(xs[0].t, 1.);
        assert_eq!(xs[0].object, p.eid());
    }

    #[test]
    fn intersect_ray_from_below() {
        let mut w = World::new();
        let p = Plane::new(&mut w);
        let r = ray(point!(0, -1, 0), vector!(0, 1, 0));
        let xs = p.local_intersect(&r, &w);
        assert_eq!(xs.len(), 1);
        assert_eq!(xs[0].t, 1.);
        assert_eq!(xs[0].object, p.eid());
    }
}
