use crate::contains::*;
use crate::eid::*;
use crate::intersect::*;
use crate::material::*;
use crate::matrix::*;
use crate::normal::*;
use crate::ray::*;
use crate::shape::ShapeStore;
use crate::tuple::*;

#[derive(Clone, Debug)]
pub struct Sphere {
    eid: Eid,
    transform: Matrix4,
    material: Material,
    parent: Option<Eid>,
}

shape_boilerplate!(Sphere);

impl HasContains for Sphere {
    fn contains(&self, eid: &Eid, _: &dyn ShapeStore) -> bool {
        self.eid == *eid
    }
}

impl Sphere {
    pub fn new(w: &mut dyn EidAllocator) -> Self {
        Sphere {
            eid: w.new_eid(),
            transform: M4_ID,
            material: Material::default(),
            parent: None,
        }
    }
}

impl HasIntersect for Sphere {
    fn local_intersect(&self, r: &Ray, _: &dyn ShapeStore) -> Vec<Intersection> {
        let sphere_to_ray = r.origin - ORIGIN;
        let a = r.direction.dot(r.direction);
        let b = 2. * r.direction.dot(sphere_to_ray);
        let c = sphere_to_ray.dot(sphere_to_ray) - 1.;
        let discriminant = b * b - 4. * a * c;
        if discriminant < 0. {
            return vec![];
        }
        let t1 = (-b - discriminant.sqrt()) / (2. * a);
        let t2 = (-b + discriminant.sqrt()) / (2. * a);
        return vec![intersection(t1, self.eid), intersection(t2, self.eid)];
    }
}

impl HasNormal for Sphere {
    fn local_normal_at(&self, p: &Point, _: &Intersection, _: &dyn ShapeStore) -> Vector {
        *p - ORIGIN
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::shape::Shape;
    use crate::transform::*;
    use crate::world::World;

    #[test]
    fn has_entity() {
        let s = Sphere::new(&mut World::new());
        let i = intersection(3.5, s.eid);
        assert_eq!(i.t, 3.5);
        assert_eq!(i.object, s.eid);
    }

    #[test]
    fn intersect_two_points() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let xs = s.intersect(&r, &w);
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].t, 4.0);
        assert_eq!(xs[1].t, 6.0);
    }

    #[test]
    fn intersect_tangent() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let r = ray(point!(0, 1, -5), vector!(0, 0, 1));
        let xs = s.intersect(&r, &w);
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].t, 5.0);
        assert_eq!(xs[1].t, 5.0);
    }

    #[test]
    fn intersect_none() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let r = ray(point!(0, 2, -5), vector!(0, 0, 1));
        let xs = s.intersect(&r, &w);
        assert_eq!(xs.len(), 0);
    }

    #[test]
    fn intersect_inside() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let r = ray(point!(0, 0, 0), vector!(0, 0, 1));
        let xs = s.intersect(&r, &w);
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].t, -1.);
        assert_eq!(xs[1].t, 1.);
    }

    #[test]
    fn intersect_behind() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let r = ray(point!(0, 0, 5), vector!(0, 0, 1));
        let xs = s.intersect(&r, &w);
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].t, -6.);
        assert_eq!(xs[1].t, -4.);
    }

    #[test]
    fn intersect_sets_same_entity() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let xs = s.intersect(&r, &w);
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].object, s.eid);
        assert_eq!(xs[1].object, s.eid);
    }

    #[test]
    fn hit_all_positive() {
        let s = Sphere::new(&mut World::new());
        let i1 = intersection(1., s.eid);
        let i2 = intersection(2., s.eid);
        assert_eq!(hit(vec![i1, i2].into_iter()), Some(i1));
    }

    #[test]
    fn hit_some_negative() {
        let s = Sphere::new(&mut World::new());
        let i1 = intersection(-1., s.eid);
        let i2 = intersection(1., s.eid);
        assert_eq!(hit(vec![i1, i2].into_iter()), Some(i2));
    }

    #[test]
    fn hit_all_negative() {
        let s = Sphere::new(&mut World::new());
        let i1 = intersection(-2., s.eid);
        let i2 = intersection(-1., s.eid);
        assert_eq!(hit(vec![i1, i2].into_iter()), None);
    }

    #[test]
    fn hit_lots() {
        let s = Sphere::new(&mut World::new());
        let i1 = intersection(5., s.eid);
        let i2 = intersection(7., s.eid);
        let i3 = intersection(-3., s.eid);
        let i4 = intersection(2., s.eid);
        assert_eq!(hit(vec![i1, i2, i3, i4].into_iter()), Some(i4));
    }

    #[test]
    fn scaled_ray_intersect() {
        let mut w = World::new();
        let mut s = Sphere::new(&mut w);
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        s.transform = scaling(2., 2., 2.);
        let xs = s.intersect(&r, &w);
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].t, 3.);
        assert_eq!(xs[1].t, 7.);
    }

    #[test]
    fn translated_ray_intersect() {
        let mut w = World::new();
        let mut s = Sphere::new(&mut w);
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        s.transform = translation(5., 0., 0.);
        let xs = s.intersect(&r, &w);
        assert_eq!(xs.len(), 0);
    }

    #[test]
    fn normal_x() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let s_eid = s.eid;
        let n = Shape::Sphere(s).normal_at(&point!(1, 0, 0), &intersection(1., s_eid), &w);
        assert_eq!(n, vector!(1, 0, 0));
    }

    #[test]
    fn normal_y() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let s_eid = s.eid;
        let n = Shape::Sphere(s).normal_at(&point!(0, 1, 0), &intersection(1., s_eid), &w);
        assert_eq!(n, vector!(0, 1, 0));
    }

    #[test]
    fn normal_z() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let s_eid = s.eid;
        let n = Shape::Sphere(s).normal_at(&point!(0, 0, 1), &intersection(1., s_eid), &w);
        assert_eq!(n, vector!(0, 0, 1));
    }

    #[test]
    fn normal_nonaxial() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let s_eid = s.eid;
        let a = SQRT_3 / 3.;
        let n = Shape::Sphere(s).normal_at(&point!(a, a, a), &intersection(1., s_eid), &w);
        assert_eq!(n, vector!(a, a, a));
    }

    #[test]
    fn normal_nonaxial_normalized() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let s_eid = s.eid;
        let a = SQRT_3 / 3.;
        let n = Shape::Sphere(s).normal_at(&point!(a, a, a), &intersection(1., s_eid), &w);
        assert_eq!(n, n.normalize());
    }
}
