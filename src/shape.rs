use crate::cone::Cone;
use crate::contains::HasContains;
use crate::csg::Csg;
use crate::cube::Cube;
use crate::cylinder::Cylinder;
use crate::eid::Eid;
use crate::entity::Entity;
use crate::group::Group;
use crate::intersect::{HasIntersect, Intersection};
use crate::material::{HasMaterial, Material};
use crate::matrix::Matrix4;
use crate::normal::HasNormal;
use crate::parent::HasParent;
use crate::plane::Plane;
use crate::ray::Ray;
use crate::sphere::Sphere;
use crate::test_shape::TestShape;
use crate::transform::HasTransform;
use crate::triangle::Triangle;
use crate::tuple::{Point, Vector};

#[derive(Debug)]
pub enum Shape {
    Cone(Cone),
    Cube(Cube),
    Csg(Csg),
    Cylinder(Cylinder),
    Group(Group),
    Plane(Plane),
    Sphere(Sphere),
    TestShape(TestShape),
    Triangle(Triangle),
}

impl Shape {
    pub fn to_cone_mut(&mut self) -> Option<&mut Cone> {
        match self {
            Self::Cone(x) => Some(x),
            _ => None,
        }
    }

    pub fn to_cube_mut(&mut self) -> Option<&mut Cube> {
        match self {
            Self::Cube(x) => Some(x),
            _ => None,
        }
    }

    pub fn to_csg_mut(&mut self) -> Option<&mut Csg> {
        match self {
            Self::Csg(x) => Some(x),
            _ => None,
        }
    }

    pub fn to_csg(&self) -> Option<&Csg> {
        match self {
            Self::Csg(x) => Some(x),
            _ => None,
        }
    }

    pub fn to_cylinder_mut(&mut self) -> Option<&mut Cylinder> {
        match self {
            Self::Cylinder(x) => Some(x),
            _ => None,
        }
    }

    pub fn to_group_mut(&mut self) -> Option<&mut Group> {
        match self {
            Self::Group(x) => Some(x),
            _ => None,
        }
    }

    pub fn to_group(&self) -> Option<&Group> {
        match self {
            Self::Group(x) => Some(x),
            _ => None,
        }
    }

    pub fn to_plane_mut(&mut self) -> Option<&mut Plane> {
        match self {
            Self::Plane(x) => Some(x),
            _ => None,
        }
    }

    pub fn to_sphere_mut(&mut self) -> Option<&mut Sphere> {
        match self {
            Self::Sphere(x) => Some(x),
            _ => None,
        }
    }

    pub fn to_triangle_mut(&mut self) -> Option<&mut Triangle> {
        match self {
            Self::Triangle(x) => Some(x),
            _ => None,
        }
    }

    pub fn to_triangle(&self) -> Option<&Triangle> {
        match self {
            Self::Triangle(x) => Some(x),
            _ => None,
        }
    }

    pub fn normal_at(&self, p: &Point, i: &Intersection, q: &dyn ShapeStore) -> Vector {
        let local_point = q.world_to_object(self, *p);
        let local_normal = self.local_normal_at(&local_point, i, q);
        q.normal_to_world(self, local_normal)
    }
}

impl Entity for Shape {
    fn eid(&self) -> Eid {
        match self {
            Self::Cone(cone) => cone.eid(),
            Self::Cube(cube) => cube.eid(),
            Self::Csg(csg) => csg.eid(),
            Self::Cylinder(cylinder) => cylinder.eid(),
            Self::Group(group) => group.eid(),
            Self::Plane(plane) => plane.eid(),
            Self::Sphere(sphere) => sphere.eid(),
            Self::TestShape(test_shape) => test_shape.eid(),
            Self::Triangle(triangle) => triangle.eid(),
        }
    }
}

impl HasContains for Shape {
    fn contains(&self, eid: &Eid, w: &dyn ShapeStore) -> bool {
        match self {
            Self::Cone(cone) => cone.contains(eid, w),
            Self::Cube(cube) => cube.contains(eid, w),
            Self::Csg(csg) => csg.contains(eid, w),
            Self::Cylinder(cylinder) => cylinder.contains(eid, w),
            Self::Group(group) => group.contains(eid, w),
            Self::Plane(plane) => plane.contains(eid, w),
            Self::Sphere(sphere) => sphere.contains(eid, w),
            Self::TestShape(test_shape) => test_shape.contains(eid, w),
            Self::Triangle(triangle) => triangle.contains(eid, w),
        }
    }
}

impl HasTransform for Shape {
    fn transform(&self) -> &Matrix4 {
        match self {
            Self::Cone(cone) => cone.transform(),
            Self::Cube(cube) => cube.transform(),
            Self::Csg(csg) => csg.transform(),
            Self::Cylinder(cylinder) => cylinder.transform(),
            Self::Group(group) => group.transform(),
            Self::Plane(plane) => plane.transform(),
            Self::Sphere(sphere) => sphere.transform(),
            Self::TestShape(test_shape) => test_shape.transform(),
            Self::Triangle(triangle) => triangle.transform(),
        }
    }

    fn set_transform(&mut self, m: Matrix4) {
        match self {
            Self::Cone(cone) => cone.set_transform(m),
            Self::Cube(cube) => cube.set_transform(m),
            Self::Csg(csg) => csg.set_transform(m),
            Self::Cylinder(cylinder) => cylinder.set_transform(m),
            Self::Group(group) => group.set_transform(m),
            Self::Plane(plane) => plane.set_transform(m),
            Self::Sphere(sphere) => sphere.set_transform(m),
            Self::TestShape(test_shape) => test_shape.set_transform(m),
            Self::Triangle(triangle) => triangle.set_transform(m),
        }
    }
}

impl HasIntersect for Shape {
    fn local_intersect(&self, r: &Ray, q: &dyn ShapeStore) -> Vec<Intersection> {
        match self {
            Self::Cone(cone) => cone.local_intersect(r, q),
            Self::Cube(cube) => cube.local_intersect(r, q),
            Self::Csg(csg) => csg.local_intersect(r, q),
            Self::Cylinder(cylinder) => cylinder.local_intersect(r, q),
            Self::Group(group) => group.local_intersect(r, q),
            Self::Plane(plane) => plane.local_intersect(r, q),
            Self::Sphere(sphere) => sphere.local_intersect(r, q),
            Self::TestShape(test_shape) => test_shape.local_intersect(r, q),
            Self::Triangle(triangle) => triangle.local_intersect(r, q),
        }
    }
}

impl HasNormal for Shape {
    fn local_normal_at(&self, p: &Point, i: &Intersection, q: &dyn ShapeStore) -> Vector {
        match self {
            Self::Cone(cone) => cone.local_normal_at(p, i, q),
            Self::Cube(cube) => cube.local_normal_at(p, i, q),
            Self::Csg(csg) => csg.local_normal_at(p, i, q),
            Self::Cylinder(cylinder) => cylinder.local_normal_at(p, i, q),
            Self::Group(group) => group.local_normal_at(p, i, q),
            Self::Plane(plane) => plane.local_normal_at(p, i, q),
            Self::Sphere(sphere) => sphere.local_normal_at(p, i, q),
            Self::TestShape(test_shape) => test_shape.local_normal_at(p, i, q),
            Self::Triangle(triangle) => triangle.local_normal_at(p, i, q),
        }
    }
}

impl HasMaterial for Shape {
    fn material(&self) -> &Material {
        match self {
            Self::Cone(cone) => cone.material(),
            Self::Cube(cube) => cube.material(),
            Self::Csg(csg) => csg.material(),
            Self::Cylinder(cylinder) => cylinder.material(),
            Self::Group(group) => group.material(),
            Self::Plane(plane) => plane.material(),
            Self::Sphere(sphere) => sphere.material(),
            Self::TestShape(test_shape) => test_shape.material(),
            Self::Triangle(triangle) => triangle.material(),
        }
    }

    fn material_mut(&mut self) -> &mut Material {
        match self {
            Self::Cone(cone) => cone.material_mut(),
            Self::Cube(cube) => cube.material_mut(),
            Self::Csg(csg) => csg.material_mut(),
            Self::Cylinder(cylinder) => cylinder.material_mut(),
            Self::Group(group) => group.material_mut(),
            Self::Plane(plane) => plane.material_mut(),
            Self::Sphere(sphere) => sphere.material_mut(),
            Self::TestShape(test_shape) => test_shape.material_mut(),
            Self::Triangle(triangle) => triangle.material_mut(),
        }
    }

    fn set_material(&mut self, m: Material) {
        match self {
            Self::Cone(cone) => cone.set_material(m),
            Self::Cube(cube) => cube.set_material(m),
            Self::Csg(csg) => csg.set_material(m),
            Self::Cylinder(cylinder) => cylinder.set_material(m),
            Self::Group(group) => group.set_material(m),
            Self::Plane(plane) => plane.set_material(m),
            Self::Sphere(sphere) => sphere.set_material(m),
            Self::TestShape(test_shape) => test_shape.set_material(m),
            Self::Triangle(triangle) => triangle.set_material(m),
        }
    }
}

impl HasParent for Shape {
    fn parent(&self) -> Option<Eid> {
        match self {
            Self::Cone(cone) => cone.parent(),
            Self::Cube(cube) => cube.parent(),
            Self::Csg(csg) => csg.parent(),
            Self::Cylinder(cylinder) => cylinder.parent(),
            Self::Group(group) => group.parent(),
            Self::Plane(plane) => plane.parent(),
            Self::Sphere(sphere) => sphere.parent(),
            Self::TestShape(test_shape) => test_shape.parent(),
            Self::Triangle(triangle) => triangle.parent(),
        }
    }

    fn set_parent(&mut self, parent: &Eid) {
        match self {
            Self::Cone(cone) => cone.set_parent(parent),
            Self::Cube(cube) => cube.set_parent(parent),
            Self::Csg(csg) => csg.set_parent(parent),
            Self::Cylinder(cylinder) => cylinder.set_parent(parent),
            Self::Group(group) => group.set_parent(parent),
            Self::Plane(plane) => plane.set_parent(parent),
            Self::Sphere(sphere) => sphere.set_parent(parent),
            Self::TestShape(test_shape) => test_shape.set_parent(parent),
            Self::Triangle(triangle) => triangle.set_parent(parent),
        }
    }
}

pub trait ShapeStore: crate::eid::EidAllocator {
    fn find_shape(&self, e: &Eid) -> Option<&Shape>;
    fn find_shape_mut(&mut self, e: &Eid) -> Option<&mut Shape>;
    fn world_to_object(&self, s: &Shape, p: Point) -> Point;
    fn normal_to_world(&self, s: &Shape, normal: Vector) -> Point;
}
