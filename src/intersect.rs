use crate::eid::*;
use crate::ray::*;
use crate::robust_eq::*;
use crate::shape::ShapeStore;
use crate::transform::*;
use crate::tuple::*;

#[derive(Clone, Copy, Debug)]
pub struct Intersection {
    pub t: f64,
    pub object: Eid,
    pub u: Option<f64>,
    pub v: Option<f64>,
}

impl Intersection {
    pub fn new(t: f64, object: Eid, u: Option<f64>, v: Option<f64>) -> Self {
        Intersection { t, object, u, v }
    }
}

pub fn intersection(t: f64, eid: Eid) -> Intersection {
    Intersection::new(t, eid, None, None)
}

pub fn intersection_with_uv(t: f64, eid: Eid, u: f64, v: f64) -> Intersection {
    Intersection::new(t, eid, Some(u), Some(v))
}

impl PartialEq for Intersection {
    fn eq(&self, other: &Self) -> bool {
        float_eq_robust(self.t, other.t) && self.object == other.object
    }
}

impl Eq for Intersection {}

impl PartialOrd for Intersection {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.t.partial_cmp(&other.t)
    }
}

impl Ord for Intersection {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self.t.partial_cmp(&other.t) {
            None => std::cmp::Ordering::Equal,
            Some(o) => o,
        }
    }
}

pub trait HasIntersect: HasTransform {
    fn intersect(&self, r: &Ray, q: &dyn ShapeStore) -> Vec<Intersection> {
        let r = r.transform(self.transform().inverse());
        self.local_intersect(&r, q)
    }

    /// Return the list of intersections with the shape.
    ///
    /// * `r` - A ray in object space.
    fn local_intersect(&self, r: &Ray, _q: &dyn ShapeStore) -> Vec<Intersection>;
}

pub fn hit(xs: impl Iterator<Item = Intersection>) -> Option<Intersection> {
    xs.filter(|&i| i.t > 0.).min()
}

pub struct IntersectComputation {
    pub t: f64,
    pub object: Eid,
    pub point: Point,
    pub eyev: Vector,
    pub normalv: Vector,
    pub inside: bool,
    /// A bit above the point in the direction of the normal. Used to avoid
    /// "acne" in the render when computing reflections.
    pub over_point: Point,
    /// A bit below the point in the direction of the normal. Used to avoid
    /// "acne" in the renderer when computing refractions.
    pub under_point: Point,
    pub reflectv: Vector,
    pub n1: f64,
    pub n2: f64,
}

impl IntersectComputation {
    pub fn schlick(&self) -> f64 {
        let mut cos = self.eyev.dot(self.normalv);
        if self.n1 > self.n2 {
            let n = self.n1 / self.n2;
            let sin2_t = n * n * (1. - cos * cos);
            if sin2_t > 1.0 {
                return 1.0;
            }
            let cos_t = (1. - sin2_t).sqrt();
            cos = cos_t;
        }
        let r0 = ((self.n1 - self.n2) / (self.n1 + self.n2)).powf(2.);
        r0 + (1. - r0) * (1. - cos).powf(5.)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::entity::Entity;
    use crate::world::World;

    #[test]
    fn prepare_computations() {
        let mut w = World::sample();
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let i = {
            let shape = &*w.add_sphere();
            intersection(4., shape.eid())
        };
        let comps = w.prepare_computations(&i, &r, &[i]);
        assert_eq!(comps.t, i.t);
        assert_eq!(comps.object, i.object);
        assert_eq!(comps.point, point!(0, 0, -1));
        assert_eq!(comps.eyev, vector!(0, 0, -1));
        assert_eq!(comps.normalv, vector!(0, 0, -1));
    }

    #[test]
    fn outside_intersection() {
        let mut w = World::sample();
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let i = {
            let shape = &*w.add_sphere();
            intersection(4.0, shape.eid())
        };
        let comps = w.prepare_computations(&i, &r, &[i]);
        assert_eq!(comps.inside, false);
    }

    #[test]
    fn inside_intersection() {
        let mut w = World::sample();
        let r = ray(ORIGIN, vector!(0, 0, 1));
        let i = {
            let shape = &*w.add_sphere();
            intersection(1.0, shape.eid())
        };
        let comps = w.prepare_computations(&i, &r, &[i]);
        assert_eq!(comps.point, point!(0, 0, 1));
        assert_eq!(comps.eyev, vector!(0, 0, -1));
        assert_eq!(comps.inside, true);
        assert_eq!(comps.normalv, vector!(0, 0, -1));
    }
}
