use crate::contains::*;
use crate::eid::*;
use crate::intersect::*;
use crate::material::*;
use crate::matrix::*;
use crate::normal::*;
use crate::ray::*;
use crate::robust_eq::*;
use crate::shape::ShapeStore;
use crate::tuple::*;

#[derive(Clone, Debug)]
pub struct Cone {
    eid: Eid,
    transform: Matrix4,
    material: Material,
    parent: Option<Eid>,
    pub minimum: f64,
    pub maximum: f64,
    pub closed: bool,
}

shape_boilerplate!(Cone);

impl HasContains for Cone {
    fn contains(&self, eid: &Eid, _: &dyn ShapeStore) -> bool {
        self.eid == *eid
    }
}

impl Cone {
    pub fn new(w: &mut dyn EidAllocator) -> Self {
        Cone {
            eid: w.new_eid(),
            transform: M4_ID,
            material: Material::default(),
            parent: None,
            minimum: std::f64::NEG_INFINITY,
            maximum: std::f64::INFINITY,
            closed: false,
        }
    }

    fn intersect_caps(&self, r: &Ray, xs: &mut Vec<Intersection>) {
        if self.closed && !float_eq_robust(r.direction.y, 0.) {
            let t = (self.minimum - r.origin.y) / r.direction.y;
            if check_cap(r, t, self.minimum) {
                xs.push(intersection(t, self.eid));
            }
            let t = (self.maximum - r.origin.y) / r.direction.y;
            if check_cap(r, t, self.maximum) {
                xs.push(intersection(t, self.eid))
            }
        }
    }
}

fn check_cap(r: &Ray, t: f64, radius: f64) -> bool {
    let x = r.origin.x + t * r.direction.x;
    let z = r.origin.z + t * r.direction.z;
    (x * x + z * z) <= radius * radius
}

impl HasIntersect for Cone {
    fn local_intersect(&self, r: &Ray, _: &dyn ShapeStore) -> Vec<Intersection> {
        let a = r.direction.x * r.direction.x - r.direction.y * r.direction.y
            + r.direction.z * r.direction.z;
        let b = 2. * r.origin.x * r.direction.x - 2. * r.origin.y * r.direction.y
            + 2. * r.origin.z * r.direction.z;
        let c = r.origin.x * r.origin.x - r.origin.y * r.origin.y + r.origin.z * r.origin.z;
        if float_eq_robust(a, 0.) {
            let mut xs = vec![];
            if !float_eq_robust(b, 0.) {
                xs.push(intersection(-c / (2. * b), self.eid));
            }
            self.intersect_caps(r, &mut xs);
            xs
        } else {
            let disc = b * b - 4. * a * c;
            if disc < 0. {
                vec![]
            } else {
                let mut t0 = (-b - disc.sqrt()) / (2. * a);
                let mut t1 = (-b + disc.sqrt()) / (2. * a);
                if t0 > t1 {
                    std::mem::swap(&mut t0, &mut t1);
                }
                let mut xs = vec![];
                let y0 = r.origin.y + t0 * r.direction.y;
                if self.minimum < y0 && y0 < self.maximum {
                    xs.push(intersection(t0, self.eid))
                }
                let y1 = r.origin.y + t1 * r.direction.y;
                if self.minimum < y1 && y1 < self.maximum {
                    xs.push(intersection(t1, self.eid))
                }
                self.intersect_caps(r, &mut xs);
                xs
            }
        }
    }
}

impl HasNormal for Cone {
    #[allow(clippy::float_cmp)]
    fn local_normal_at(&self, p: &Point, _: &Intersection, _: &dyn ShapeStore) -> Vector {
        let dist = p.x * p.x + p.z * p.z;
        if dist < 1. && p.y >= self.maximum - 1e-5 {
            vector!(0, 1, 0)
        } else if dist < 1. && p.y <= self.minimum + 1e-5 {
            vector!(0, -1, 0)
        } else {
            let mut y = (p.x * p.x + p.z * p.z).sqrt();
            if p.y > 0. {
                y = -y;
            }
            vector!(p.x, y, p.z)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::world::World;

    #[test]
    fn intersecting_cone_with_ray() {
        let mut w = World::new();
        let shape = Cone::new(&mut w);
        let cases = [
            (point!(0, 0, -5), vector!(0, 0, 1), 5., 5.),
            (point!(0, 0, -5), vector!(1, 1, 1), 8.66025, 8.66025),
            (point!(1, 1, -5), vector!(-0.5, -1, 1), 4.55006, 49.44994),
        ];
        for (i, (origin, direction, t0, t1)) in cases.iter().enumerate() {
            let direction = direction.normalize();
            let r = ray(*origin, direction);
            let xs = shape.local_intersect(&r, &w);
            assert_eq!(xs.len(), 2, "Test #{}", i);
            assert!(
                float_eq_robust(xs[0].t, *t0),
                "Test #{}: {} == {}",
                i,
                xs[0].t,
                t0
            );
            assert!(
                float_eq_robust(xs[1].t, *t1),
                "Test #{}: {} == {}",
                i,
                xs[1].t,
                t1
            );
        }
    }

    #[test]
    fn intersect_cone_with_parallel_ray() {
        let mut w = World::new();
        let shape = Cone::new(&mut w);
        let r = ray(point!(0, 0, -1), vector!(0, 1, 1).normalize());
        let xs = shape.local_intersect(&r, &w);
        assert_eq!(xs.len(), 1);
        assert!(float_eq_robust(xs[0].t, 0.35355));
    }

    #[test]
    fn intersect_ray_with_cone_caps() {
        let mut w = World::new();
        let mut shape = Cone::new(&mut w);
        shape.minimum = -0.5;
        shape.maximum = 0.5;
        shape.closed = true;
        let cases = [
            (point!(0, 0, -5), vector!(0, 1, 0), 0),
            (point!(0, 0, -0.25), vector!(0, 1, 1), 2),
            (point!(0, 0, -0.25), vector!(0, 1, 0), 4),
        ];
        for (i, (origin, direction, count)) in cases.iter().enumerate() {
            let direction = direction.normalize();
            let r = ray(*origin, direction);
            let xs = shape.local_intersect(&r, &w);
            assert_eq!(xs.len(), *count, "Test #{}", i);
        }
    }

    #[test]
    fn normal_of_cone() {
        let mut w = World::new();
        let shape = Cone::new(&mut w);
        let cases = [
            (point!(0, 0, 0), vector!(0, 0, 0)),
            (point!(1, 1, 1), vector!(1, -SQRT_2, 1)),
            (point!(-1, -1, 0), vector!(-1, 1, 0)),
        ];
        for (i, (point, normal)) in cases.iter().enumerate() {
            let n = shape.local_normal_at(
                point,
                &intersection((*point - ORIGIN).magnitude(), shape.eid),
                &w,
            );
            assert_eq!(n, *normal, "Test #{}", i);
        }
    }
}
