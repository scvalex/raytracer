use crate::eid::*;

pub trait HasParent {
    fn parent(&self) -> Option<Eid>;

    fn set_parent(&mut self, parent: &Eid);
}
