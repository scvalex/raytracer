use crate::color::*;
use crate::tuple::*;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Light {
    PointLight { position: Point, intensity: Color },
}

use Light::*;

impl Light {
    pub fn position(&self) -> &Point {
        match self {
            PointLight { position, .. } => position,
        }
    }

    pub fn intensity(&self) -> &Color {
        match self {
            PointLight { intensity, .. } => intensity,
        }
    }
}

pub fn point_light(position: Point, intensity: Color) -> Light {
    PointLight {
        position,
        intensity,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn light_has_position_and_intensity() {
        let intensity = WHITE;
        let position = ORIGIN;
        let light = point_light(position, intensity);
        assert_eq!(light.position(), &position);
        assert_eq!(light.intensity(), &intensity);
    }
}
