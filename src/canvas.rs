use crate::color::*;
use std::fs::File;
use std::io::BufWriter;
use std::path::Path;

pub struct Canvas {
    pub width: usize,
    pub height: usize,
    pub pixels: Vec<Vec<Color>>,
}

pub fn canvas(width: usize, height: usize) -> Canvas {
    let mut pixels = Vec::with_capacity(width);
    for i in 0..width {
        pixels.push(Vec::with_capacity(height));
        pixels[i].resize(height, BLACK);
    }
    Canvas {
        width,
        height,
        pixels,
    }
}

impl Canvas {
    pub fn pixel_at(&self, x: usize, y: usize) -> Color {
        assert!(x < self.width && y < self.height);
        self.pixels[x][y]
    }

    pub fn write_pixel(&mut self, x: usize, y: usize, col: Color) {
        assert!(x < self.width && y < self.height);
        self.pixels[x][y] = col;
    }

    pub fn write_to_file(&self, path: &str) -> Result<(), failure::Error> {
        let path = Path::new(path);
        let file = File::create(path)?;
        let w = BufWriter::new(file);
        let mut encoder = png::Encoder::new(w, self.width as u32, self.height as u32);
        encoder.set_color(png::ColorType::RGB);
        encoder.set_depth(png::BitDepth::Eight);
        let mut writer = encoder.write_header()?;
        let mut data = Vec::with_capacity(self.width * self.height * 3);
        for j in 0..self.height {
            for i in 0..self.width {
                let (r, g, b) = self.pixels[i][self.height - j - 1].to_8bit();
                data.push(r);
                data.push(g);
                data.push(b);
            }
        }
        writer.write_image_data(&data)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn canvas_test() {
        let mut c = canvas(10, 20);
        for i in 0..10 {
            for j in 0..20 {
                assert_eq!(c.pixel_at(i, j), BLACK);
            }
        }
        c.write_pixel(2, 3, RED);
        assert_eq!(c.pixel_at(2, 3), RED);
    }
}
