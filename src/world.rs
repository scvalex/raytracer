use crate::color::*;
use crate::cone::*;
use crate::csg::*;
use crate::cube::*;
use crate::cylinder::*;
use crate::eid::*;
use crate::entity::*;
use crate::group::*;
use crate::intersect::*;
use crate::light::*;
use crate::material::HasMaterial;
use crate::material::*;
use crate::parent::HasParent;
use crate::plane::*;
use crate::ray::*;
use crate::shape::{Shape, ShapeStore};
use crate::sphere::*;
use crate::transform::*;
use crate::triangle::*;
use crate::tuple::*;
use std::borrow::Borrow;
use std::collections::HashMap;

#[derive(Debug)]
pub struct World {
    pub eid_counter: usize,
    pub shapes: HashMap<Eid, Shape>,
    pub light: Light,
}

impl EidAllocator for World {
    fn new_eid_usize(&mut self) -> usize {
        let eid = self.eid_counter;
        self.eid_counter += 1;
        eid
    }
}

impl World {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        World {
            eid_counter: 1,
            shapes: HashMap::new(),
            light: point_light(point!(-10, 10, -10), WHITE),
        }
    }

    pub fn sample() -> World {
        let mut w = World::new();
        {
            let s1 = w.add_sphere();
            s1.set_material({
                let mut m = material();
                m.color = color!(0.8, 1, 0.6);
                m.diffuse = 0.7;
                m.specular = 0.2;
                m
            });
        }
        {
            let s2 = w.add_sphere();
            s2.set_transform(scaling(0.5, 0.5, 0.5));
        }
        w
    }

    pub fn add_sphere(&mut self) -> &mut Sphere {
        let s = Sphere::new(self);
        self.shapes
            .entry(s.eid())
            .or_insert_with(|| Shape::Sphere(s))
            .to_sphere_mut()
            .unwrap()
    }

    pub fn add_glass_sphere(&mut self) -> &mut Sphere {
        let mut s = Sphere::new(self);
        s.set_material(Material::new_glass());
        self.shapes
            .entry(s.eid())
            .or_insert_with(|| Shape::Sphere(s))
            .to_sphere_mut()
            .unwrap()
    }

    pub fn add_plane(&mut self) -> &mut Plane {
        let p = Plane::new(self);
        self.shapes
            .entry(p.eid())
            .or_insert_with(|| Shape::Plane(p))
            .to_plane_mut()
            .unwrap()
    }

    pub fn add_cube(&mut self) -> &mut Cube {
        let c = Cube::new(self);
        self.shapes
            .entry(c.eid())
            .or_insert_with(|| Shape::Cube(c))
            .to_cube_mut()
            .unwrap()
    }

    pub fn add_cylinder(&mut self) -> &mut Cylinder {
        let c = Cylinder::new(self);
        self.shapes
            .entry(c.eid())
            .or_insert_with(|| Shape::Cylinder(c))
            .to_cylinder_mut()
            .unwrap()
    }

    pub fn add_cone(&mut self) -> &mut Cone {
        let c = Cone::new(self);
        self.shapes
            .entry(c.eid())
            .or_insert_with(|| Shape::Cone(c))
            .to_cone_mut()
            .unwrap()
    }

    pub fn add_triangle(&mut self, p1: Point, p2: Point, p3: Point) -> &mut Triangle {
        let t = Triangle::new(self, p1, p2, p3);
        self.shapes
            .entry(t.eid())
            .or_insert_with(|| Shape::Triangle(t))
            .to_triangle_mut()
            .unwrap()
    }

    pub fn add_csg(&mut self, left: Eid, right: Eid, operation: Operation) -> &mut Csg {
        let c = Csg::new(self, left, right, operation);
        self.shapes
            .entry(c.eid())
            .or_insert_with(|| Shape::Csg(c))
            .to_csg_mut()
            .unwrap()
    }

    pub fn add_group(&mut self) -> &mut Group {
        let g = Group::new(self);
        self.shapes
            .entry(g.eid())
            .or_insert_with(|| Shape::Group(g))
            .to_group_mut()
            .unwrap()
    }

    pub fn add_shape(&mut self, s: Shape) {
        self.shapes.entry(s.eid()).or_insert_with(|| s);
    }

    pub fn prepare_computations(
        &self,
        hit: &Intersection,
        r: &Ray,
        intersections: &[Intersection],
    ) -> IntersectComputation {
        // This unwrap never raises because the object was
        // previously found in the world.
        let shape: &Shape = self.find_shape(&hit.object).unwrap();
        let point = r.position(hit.t);
        let normalv = shape.normal_at(&point, hit, self);
        let eyev = -r.direction;
        let (inside, normalv) = if normalv.dot(eyev) < 0. {
            (true, -normalv)
        } else {
            (false, normalv)
        };
        let mut n1 = REFRACTIVE_AIR;
        let mut n2 = REFRACTIVE_AIR;
        {
            let mut containers: Vec<Eid> = vec![];
            for i in intersections {
                if i == hit {
                    match containers.last() {
                        None => n1 = REFRACTIVE_AIR,
                        Some(last) => {
                            // This unwrap never raises because the object was
                            // previously found in the world.
                            n1 = self.find_shape(last).unwrap().material().refractive_index;
                        }
                    }
                }
                if containers.iter().any(|container| *container == i.object) {
                    containers = containers
                        .into_iter()
                        .filter(|container| *container != i.object)
                        .collect();
                } else {
                    containers.push(i.object);
                }
                if i == hit {
                    match containers.last() {
                        None => n2 = REFRACTIVE_AIR,
                        Some(last) => {
                            // This unwrap never raises because the object was
                            // previously found in the world.
                            n2 = self.find_shape(last).unwrap().material().refractive_index;
                        }
                    }
                }
            }
        };
        IntersectComputation {
            t: hit.t,
            object: hit.object,
            point,
            eyev,
            normalv,
            inside,
            over_point: point + normalv * 1e-5,
            under_point: point - normalv * 1e-5,
            reflectv: r.direction.reflect(&normalv),
            n1,
            n2,
        }
    }

    pub fn shade_hit(&self, comps: impl Borrow<IntersectComputation>, remaining: i64) -> Color {
        let comps = comps.borrow();
        let shape = {
            // This never panics because the intersected object was found in
            // this world.
            self.find_shape(&comps.object).unwrap()
        };
        let surface = shape.material().lighting(
            shape,
            &self.light,
            &comps.point,
            &comps.eyev,
            &comps.normalv,
            self.is_shadowed(&comps.over_point),
            self,
        );
        let reflected = self.reflected_color(comps, remaining);
        let refracted = self.refracted_color(comps, remaining);
        if shape.material().reflective > 0. && shape.material().transparency > 0. {
            let reflectance = comps.schlick();
            surface + reflected * reflectance + refracted * (1. - reflectance)
        } else {
            surface + reflected + refracted
        }
    }

    pub fn color_at(&self, r: &Ray, remaining: i64) -> Color {
        match hit(self.intersect(r).into_iter()) {
            None => BLACK,
            Some(i) => {
                let comps = self.prepare_computations(&i, r, &[i]);
                self.shade_hit(comps, remaining)
            }
        }
    }

    pub fn is_shadowed(&self, p: &Point) -> bool {
        let light_to_point = *self.light.position() - *p;
        let distance = light_to_point.magnitude();
        let shadow_ray = ray(*p, light_to_point.normalize());

        let opaque_intersections = self
            .intersect(&shadow_ray)
            .into_iter()
            .filter(|intersection| {
                // This unwrap never fails beause the intersection found the object
                // originally.
                let shape = self.find_shape(&intersection.object).unwrap();
                shape.material().casts_shadow
            });
        match hit(opaque_intersections) {
            None => false,
            Some(i) => i.t <= distance,
        }
    }

    fn intersect(&self, r: &Ray) -> Vec<Intersection> {
        let mut res = vec![];
        for s in self.shapes.values() {
            match s.parent() {
                None => {
                    let mut intersections = s.intersect(r, self);
                    res.append(&mut intersections);
                }
                Some(_) => {
                    // Only check for intersections for top-level objects.
                    // Objects with parents rely on their parents to check them
                    // for intersections.
                    ()
                }
            };
        }
        res.sort();
        res
    }

    pub fn reflected_color(&self, comps: &IntersectComputation, remaining: i64) -> Color {
        if remaining < 1 {
            BLACK
        } else {
            // This never fails because the intersection found the object.
            let shape = self.find_shape(&comps.object).unwrap();
            if shape.material().reflective == 0. {
                BLACK
            } else {
                let reflect_ray = ray(comps.over_point, comps.reflectv);
                let color = self.color_at(&reflect_ray, remaining - 1);
                color * shape.material().reflective
            }
        }
    }

    pub fn refracted_color(&self, comps: &IntersectComputation, remaining: i64) -> Color {
        if remaining < 1 {
            BLACK
        } else {
            // This never fails because the intersection found the object.
            let shape = self.find_shape(&comps.object).unwrap();
            if shape.material().transparency == 0. {
                BLACK
            } else {
                let n_ratio = comps.n1 / comps.n2;
                let cos_i = comps.eyev.dot(comps.normalv);
                let sin2_t = n_ratio * n_ratio * (1. - cos_i * cos_i);
                if sin2_t > 1. {
                    BLACK
                } else {
                    let cos_t = (1. - sin2_t).sqrt();
                    let direction =
                        comps.normalv * (n_ratio * cos_i - cos_t) - comps.eyev * n_ratio;
                    let refract_ray = ray(comps.under_point, direction);
                    self.color_at(&refract_ray, remaining - 1) * shape.material().transparency
                }
            }
        }
    }
}

impl ShapeStore for World {
    fn find_shape(&self, e: &Eid) -> Option<&Shape> {
        Some(self.shapes.get(e)?)
    }

    fn find_shape_mut(&mut self, e: &Eid) -> Option<&mut Shape> {
        Some(self.shapes.get_mut(e)?)
    }

    fn world_to_object(&self, s: &Shape, mut p: Point) -> Point {
        if let Some(parent) = s.parent() {
            p = self.world_to_object(self.find_shape(&parent).unwrap(), p);
        }
        s.transform().inverse() * p
    }

    fn normal_to_world(&self, s: &Shape, mut normal: Vector) -> Point {
        normal = s.transform().inverse().transpose() * normal;
        normal.w = 0.;
        normal = normal.normalize();
        if let Some(parent) = s.parent() {
            normal = self.normal_to_world(self.find_shape(&parent).unwrap(), normal);
        }
        normal
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::matrix::*;
    use crate::robust_eq::*;

    #[test]
    fn intersect() {
        let w = World::sample();
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let xs = w.intersect(&r);
        assert_eq!(xs.len(), 4);
        assert_eq!(xs[0].t, 4.);
        assert_eq!(xs[1].t, 4.5);
        assert_eq!(xs[2].t, 5.5);
        assert_eq!(xs[3].t, 6.0);
    }

    #[test]
    fn shade_intersection() {
        let w = World::sample();
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let mut keys: Vec<&Eid> = w.shapes.keys().collect();
        keys.sort();
        let shape = w.find_shape(keys[0]).unwrap();
        let i = intersection(4.0, shape.eid());
        let comps = w.prepare_computations(&i, &r, &[i]);
        let c = w.shade_hit(comps, 0);
        assert_eq!(c, color!(0.38066, 0.47583, 0.2855));
    }

    #[test]
    fn shade_intersection_from_inside() {
        let mut w = World::sample();
        w.light = point_light(point!(0, 0.25, 0), WHITE);
        let r = ray(ORIGIN, vector!(0, 0, 1));
        let mut keys: Vec<&Eid> = w.shapes.keys().collect();
        keys.sort();
        let shape = w.find_shape(keys[1]).unwrap();
        let i = intersection(0.5, shape.eid());
        let comps = w.prepare_computations(&i, &r, &[i]);
        let c = w.shade_hit(comps, 0);
        assert_eq!(c, color!(0.90498, 0.90498, 0.90498));
    }

    #[test]
    fn ray_misses() {
        let w = World::sample();
        let r = ray(point!(0, 0, -5), vector!(0, 1, 0));
        let c = w.color_at(&r, 0);
        assert_eq!(c, BLACK);
    }

    #[test]
    fn ray_hits() {
        let w = World::sample();
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let c = w.color_at(&r, 0);
        assert_eq!(c, color!(0.38066, 0.47583, 0.2855));
    }

    #[test]
    fn ray_hit_behind() {
        let mut w = World::sample();
        let (key_outer, key_inner) = {
            let mut keys: Vec<&Eid> = w.shapes.keys().collect();
            keys.sort();
            (keys[0].clone(), keys[1].clone())
        };
        {
            let outer: &mut Sphere = w
                .find_shape_mut(&key_outer)
                .unwrap()
                .to_sphere_mut()
                .unwrap();
            outer.material_mut().ambient = 1.;
        }
        let inner_color = {
            let inner: &mut Sphere = w
                .find_shape_mut(&key_inner)
                .unwrap()
                .to_sphere_mut()
                .unwrap();
            inner.material_mut().ambient = 1.;
            inner.material().color
        };
        let r = ray(point!(0, 0, 0.75), vector!(0, 0, -1));
        let c = w.color_at(&r, 0);
        assert_eq!(c, inner_color);
    }

    #[test]
    fn is_shadowed_not() {
        let w = World::sample();
        let p = point!(0, 10, 0);
        assert_eq!(w.is_shadowed(&p), false);
    }

    #[test]
    fn is_shadowed_far_side() {
        let w = World::sample();
        let p = point!(10, -10, 10);
        assert_eq!(w.is_shadowed(&p), true);
    }

    #[test]
    fn is_shadowed_light_between() {
        let w = World::sample();
        let p = point!(-20, 20, -20);
        assert_eq!(w.is_shadowed(&p), false);
    }

    #[test]
    fn is_shadowed_point_between() {
        let w = World::sample();
        let p = point!(-2, 2, -2);
        assert_eq!(w.is_shadowed(&p), false);
    }

    #[test]
    fn shade_hit_in_shadow() {
        let mut w = World::new();
        w.light = point_light(point!(0, 0, -10), WHITE);
        {
            let _: &mut Sphere = w.add_sphere();
        }
        let i = {
            let s = w.add_sphere();
            s.set_transform(translation(0., 0., 10.));
            intersection(4., s.eid())
        };
        let r = ray(point!(0, 0, 5), vector!(0, 0, 1));
        let comps = w.prepare_computations(&i, &r, &[i]);
        let c = w.shade_hit(&comps, 0);
        assert_eq!(c, color!(0.1, 0.1, 0.1));
    }

    #[test]
    fn shade_hit_above_object() {
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let mut w = World::new();
        let i = {
            let s = w.add_sphere();
            s.set_transform(translation(0., 0., 1.));
            intersection(5., s.eid())
        };
        let comps = w.prepare_computations(&i, &r, &[i]);
        assert!(comps.over_point.z < -1e-5 / 2.0);
        assert!(comps.point.z > comps.over_point.z);
    }

    #[test]
    fn under_point_is_below_the_surface() {
        let mut w = World::new();
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let eid = {
            let shape = w.add_glass_sphere();
            shape.set_transform(translation(0., 0., 1.));
            shape.eid()
        };
        let i = intersection(5., eid);
        let comps = w.prepare_computations(&i, &r, &[i]);
        assert!(comps.under_point.z > 1e-5 / 2.);
        assert!(comps.point.z < comps.under_point.z);
    }

    #[test]
    fn precomputing_reflection_vector() {
        let mut w = World::new();
        let s = w.add_plane();
        let r = ray(point!(0, 1, -1), vector!(0, -SQRT_2 / 2., SQRT_2 / 2.));
        let i = intersection(SQRT_2, s.eid());
        let comps = w.prepare_computations(&i, &r, &[i]);
        assert_eq!(comps.reflectv, vector!(0, SQRT_2 / 2., SQRT_2 / 2.));
    }

    #[test]
    fn strike_nonreflective_surface() {
        let mut w = World::sample();
        let r = ray(ORIGIN, vector!(0, 0, 1));
        let eid = w.shapes.keys().nth(1).unwrap().clone();
        let sphere: &mut Sphere = w.find_shape_mut(&eid).unwrap().to_sphere_mut().unwrap();
        sphere.material_mut().ambient = 1.;
        let i = intersection(1., eid);
        let comps = w.prepare_computations(&i, &r, &[i]);
        assert_eq!(w.reflected_color(&comps, 1), BLACK);
    }

    #[test]
    fn strike_reflective_surface() {
        let mut w = World::sample();
        let plane = w.add_plane();
        plane.material_mut().reflective = 0.5;
        plane.set_transform(translation(0., -1., 0.));
        let r = ray(point!(0, 0, -3), vector!(0, -SQRT_2 / 2., SQRT_2 / 2.));
        let i = intersection(SQRT_2, plane.eid());
        let comps = w.prepare_computations(&i, &r, &[i]);
        assert_eq!(
            w.shade_hit(&comps, 1),
            color!(0.8767577, 0.924340, 0.829174)
        );
    }

    #[test]
    fn strike_reflective_surface_no_more_steps_remaining() {
        let mut w = World::sample();
        let plane = w.add_plane();
        plane.material_mut().reflective = 0.5;
        plane.set_transform(translation(0., -1., 0.));
        let r = ray(point!(0, 0, -3), vector!(0, -SQRT_2 / 2., SQRT_2 / 2.));
        let i = intersection(SQRT_2, plane.eid());
        let comps = w.prepare_computations(&i, &r, &[i]);
        assert_eq!(w.reflected_color(&comps, 0), BLACK);
    }

    #[test]
    fn color_at_mutually_reflective_surfaces() {
        let mut w = World::new();
        w.light = point_light(ORIGIN, WHITE);
        {
            let lower = w.add_plane();
            lower.material_mut().reflective = 1.;
            lower.set_transform(translation(0., -1., 0.));
        }
        {
            let upper = w.add_plane();
            upper.material_mut().reflective = 1.;
            upper.set_transform(translation(0., 1., 0.));
        }
        let r = ray(ORIGIN, vector!(0, 1, 0));
        let _ = w.color_at(&r, 0);
    }

    #[test]
    fn glass_sphere_is_made_of_glass() {
        let mut w = World::new();
        let s = w.add_glass_sphere();
        assert_eq!(*s.transform(), M4_ID);
        assert_eq!(s.material().transparency, 1.);
        assert_eq!(s.material().refractive_index, REFRACTIVE_GLASS);
    }

    #[test]
    fn finding_n1_and_n2_at_various_intersections() {
        let mut w = World::new();
        let eid_a = {
            let a = w.add_glass_sphere();
            a.set_transform(scaling(2., 2., 2.));
            a.material_mut().refractive_index = 1.5;
            a.eid()
        };
        let eid_b = {
            let b = w.add_glass_sphere();
            b.set_transform(translation(0., 0., -0.25));
            b.material_mut().refractive_index = 2.0;
            b.eid()
        };
        let eid_c = {
            let c = w.add_glass_sphere();
            c.set_transform(translation(0., 0., 0.25));
            c.material_mut().refractive_index = 2.5;
            c.eid()
        };
        let r = ray(point!(0, 0, -4), vector!(0, 0, 1));
        let xs: Vec<Intersection> = [
            (2., eid_a),
            (2.75, eid_b),
            (3.25, eid_c),
            (4.75, eid_b),
            (5.25, eid_c),
            (6., eid_a),
        ]
        .iter()
        .map(|(t, eid)| intersection(*t, *eid))
        .collect();
        let expected_n = [
            (REFRACTIVE_AIR, 1.5),
            (1.5, 2.0),
            (2.0, 2.5),
            (2.5, 2.5),
            (2.5, 1.5),
            (1.5, REFRACTIVE_AIR),
        ];
        for (intersection, (expected_n1, expected_n2)) in xs.iter().zip(expected_n.iter()) {
            let comps = w.prepare_computations(&intersection, &r, &xs);
            assert_eq!(
                comps.n1, *expected_n1,
                "n1 at intersection {:?}",
                intersection
            );
            assert_eq!(
                comps.n2, *expected_n2,
                "n2 at intersection {:?}",
                intersection
            );
        }
    }

    #[test]
    fn refracted_color_on_opaque_surface() {
        let w = World::sample();
        let eid = w.shapes.keys().nth(0).unwrap();
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let xs = [intersection(4., *eid), intersection(6., *eid)];
        let comps = w.prepare_computations(&xs[0], &r, &xs);
        assert_eq!(w.refracted_color(&comps, 5), BLACK);
    }

    #[test]
    fn refracted_color_on_transparent_surface_at_maximum_depth() {
        let mut w = World::sample();
        let eid = w.shapes.keys().nth(0).unwrap().clone();
        {
            let s: &mut Sphere = w.find_shape_mut(&eid).unwrap().to_sphere_mut().unwrap();
            s.material_mut().transparency = 1.;
            s.material_mut().refractive_index = 1.5;
        }
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let xs = [intersection(4., eid), intersection(6., eid)];
        let comps = w.prepare_computations(&xs[0], &r, &xs);
        assert_eq!(w.refracted_color(&comps, 0), BLACK);
    }

    #[test]
    fn refraction_under_total_internal_reflection() {
        let mut w = World::sample();
        let eid = w.shapes.keys().nth(0).unwrap().clone();
        {
            let s: &mut Sphere = w.find_shape_mut(&eid).unwrap().to_sphere_mut().unwrap();
            s.material_mut().transparency = 1.;
            s.material_mut().refractive_index = 1.5;
        }
        let r = ray(point!(0, 0, -SQRT_2 / 2.), vector!(0, 1, 0));
        let xs = [
            intersection(-SQRT_2 / 2., eid),
            intersection(SQRT_2 / 2., eid),
        ];
        let comps = w.prepare_computations(&xs[1], &r, &xs);
        assert_eq!(w.refracted_color(&comps, 5), BLACK);
    }

    #[test]
    fn refracted_color_with_refracted_ray() {
        use crate::pattern::abstract_pattern_tests::TestPattern;
        use std::rc::Rc;
        let mut w = World::sample();
        let (eid_a, eid_b) = {
            let mut keys: Vec<&Eid> = w.shapes.keys().collect();
            keys.sort();
            (keys[0].clone(), keys[1].clone())
        };
        {
            let s: &mut Sphere = w.find_shape_mut(&eid_a).unwrap().to_sphere_mut().unwrap();
            s.material_mut().ambient = 1.;
            s.material_mut().pattern = Some(Rc::new(TestPattern::new()));
        }
        {
            let s: &mut Sphere = w.find_shape_mut(&eid_b).unwrap().to_sphere_mut().unwrap();
            s.material_mut().transparency = 1.;
            s.material_mut().refractive_index = 1.5;
        }
        let r = ray(point!(0, 0, 0.1), vector!(0, 1, 0));
        let xs = [
            intersection(-0.9899, eid_a),
            intersection(-0.4899, eid_b),
            intersection(0.4899, eid_b),
            intersection(0.9899, eid_a),
        ];
        let comps = w.prepare_computations(&xs[2], &r, &xs);
        assert_eq!(w.refracted_color(&comps, 5), color!(0, 0.998884, 0.047219));
    }

    #[test]
    fn shade_hit_with_transparent_material() {
        let mut w = World::sample();
        let floor = {
            let floor = w.add_plane();
            floor.set_transform(translation(0., -1., 0.));
            floor.material_mut().transparency = 0.5;
            floor.material_mut().refractive_index = 1.5;
            floor.eid()
        };
        {
            let ball = w.add_sphere();
            ball.material_mut().color = RED;
            ball.material_mut().ambient = 0.5;
            ball.set_transform(translation(0., -3.5, -0.5));
        }
        let r = ray(point!(0, 0, -3), vector!(0, -SQRT_2 / 2., SQRT_2 / 2.));
        let xs = [intersection(SQRT_2, floor)];
        let comps = w.prepare_computations(&xs[0], &r, &xs);
        assert_eq!(w.shade_hit(&comps, 5), color!(0.93642, 0.68642, 0.68642));
    }

    #[test]
    fn schlick_approximation_under_total_reflection() {
        let mut w = World::new();
        let shape = w.add_glass_sphere();
        let r = ray(point!(0, 0, SQRT_2 / 2.), vector!(0, 1, 0));
        let xs = [
            intersection(-SQRT_2 / 2., shape.eid()),
            intersection(SQRT_2 / 2., shape.eid()),
        ];
        let comps = w.prepare_computations(&xs[1], &r, &xs);
        assert_eq!(comps.schlick(), 1.0);
    }

    #[test]
    fn schlick_approximation_under_perpendicular_viewing_angle() {
        let mut w = World::new();
        let shape = w.add_glass_sphere();
        let r = ray(ORIGIN, vector!(0, 1, 0));
        let xs = [
            intersection(-1., shape.eid()),
            intersection(1., shape.eid()),
        ];
        let comps = w.prepare_computations(&xs[1], &r, &xs);
        assert!(float_eq_robust(comps.schlick(), 0.0425227));
    }

    #[test]
    fn schlick_approximation_when_n2_gt_n1() {
        let mut w = World::new();
        let shape = w.add_glass_sphere();
        let r = ray(point!(0, 0.99, -2), vector!(0, 0, 1));
        let xs = [intersection(1.8589, shape.eid())];
        let comps = w.prepare_computations(&xs[0], &r, &xs);
        assert!(float_eq_robust(comps.schlick(), 0.490074));
    }

    #[test]
    fn shade_hit_with_a_reflective_and_transparent_material() {
        let mut w = World::sample();
        let r = ray(point!(0, 0, -3), vector!(0, -SQRT_2 / 2., SQRT_2 / 2.));
        let floor = {
            let floor = w.add_plane();
            floor.set_transform(translation(0., -1., 0.));
            let mut m = floor.material_mut();
            m.reflective = 0.5;
            m.transparency = 0.5;
            m.refractive_index = 1.5;
            floor.eid()
        };
        {
            let ball = w.add_sphere();
            ball.set_transform(translation(0., -3.5, -0.5));
            let mut m = ball.material_mut();
            m.color = RED;
            m.ambient = 0.5;
        }
        let xs = [intersection(SQRT_2, floor)];
        let comps = w.prepare_computations(&xs[0], &r, &xs);
        assert_eq!(w.shade_hit(&comps, 5), color!(0.93391, 0.69643, 0.69243));
    }
}

#[cfg(test)]
mod abstract_shape_tests {
    use super::*;
    use crate::matrix::M4_ID;
    use crate::test_shape::TestShape;

    #[test]
    fn object_default_transform() {
        let mut w = World::sample();
        let test_shape = TestShape::new(&mut w);
        assert_eq!(*test_shape.transform(), M4_ID);
    }

    #[test]
    fn object_change_transform() {
        let mut w = World::sample();
        let mut test_shape = TestShape::new(&mut w);
        let t = translation(2., 3., 4.);
        test_shape.set_transform(t);
        assert_eq!(*test_shape.transform(), t);
    }

    #[test]
    fn object_default_material() {
        let mut w = World::sample();
        let test_shape = TestShape::new(&mut w);
        assert_eq!(*test_shape.material(), material());
    }

    #[test]
    fn object_change_material() {
        let mut w = World::sample();
        let mut test_shape = TestShape::new(&mut w);
        let mut m = material();
        m.ambient = 1.;
        test_shape.set_material(m.clone());
        assert_eq!(*test_shape.material(), m);
    }

    #[test]
    fn object_scaled_intersect_ray() {
        let mut w = World::sample();
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let mut test_shape = TestShape::new(&mut w);
        test_shape.set_transform(scaling(2., 2., 2.));
        let _: Vec<Intersection> = test_shape.intersect(&r, &w);
        assert_eq!(
            test_shape.saved_ray().map(|r| r.origin),
            Some(point!(0, 0, -2.5))
        );
        assert_eq!(
            test_shape.saved_ray().map(|r| r.direction),
            Some(vector!(0, 0, 0.5))
        );
    }

    #[test]
    fn object_translated_intersect_ray() {
        let mut w = World::new();
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let mut test_shape = TestShape::new(&mut w);
        test_shape.set_transform(translation(5., 0., 0.));
        let _: Vec<Intersection> = test_shape.intersect(&r, &w);
        assert_eq!(
            test_shape.saved_ray().map(|r| r.origin),
            Some(point!(-5, 0, -5))
        );
        assert_eq!(
            test_shape.saved_ray().map(|r| r.direction),
            Some(vector!(0, 0, 1))
        );
    }

    #[test]
    fn object_translated_normal() {
        let mut w = World::new();
        let mut test_shape = TestShape::new(&mut w);
        let test_shape_eid = test_shape.eid();
        test_shape.set_transform(translation(0., 1., 0.));
        let n = Shape::TestShape(test_shape).normal_at(
            &point!(0, 1.70711, -0.70711),
            &intersection(0., test_shape_eid),
            &w,
        );
        assert_eq!(n, vector!(0, 0.70711, -0.70711));
    }

    #[test]
    fn object_transformed_normal() {
        let mut w = World::new();
        let mut test_shape = TestShape::new(&mut w);
        let test_shape_eid = test_shape.eid();
        test_shape.set_transform(scaling(1.0, 0.5, 1.0) * rotation_z(PI / 5.));
        let n = Shape::TestShape(test_shape).normal_at(
            &point!(0, SQRT_2 / 2., -SQRT_2 / 2.),
            &intersection(0., test_shape_eid),
            &w,
        );
        assert_eq!(n, vector!(0, 0.97014, -0.24254));
    }

    #[test]
    fn new_object_has_no_parent() {
        let mut w = World::new();
        let test_shape = TestShape::new(&mut w);
        assert_eq!(test_shape.parent(), None);
    }

    #[test]
    fn add_child_to_group() {
        let mut world = World::new();
        let mut group = Group::new(&mut world);
        let mut s = TestShape::new(&mut world);
        group.add_child(&mut s);
        assert_eq!(group.members.len(), 1);
        assert_eq!(s.parent(), Some(group.eid()));
    }

    #[test]
    fn convert_point_from_world_to_object_space() {
        let mut w = World::new();
        let mut g1 = Group::new(&mut w);
        g1.set_transform(rotation_y(PI / 2.));
        let mut g2 = Group::new(&mut w);
        g2.set_transform(scaling(2., 2., 2.));
        g1.add_child(&mut g2);
        let mut s = Sphere::new(&mut w);
        s.set_transform(translation(5., 0., 0.));
        let s_eid = s.eid();
        g2.add_child(&mut s);
        w.add_shape(Shape::Group(g1));
        w.add_shape(Shape::Group(g2));
        w.add_shape(Shape::Sphere(s));
        let p = w.world_to_object(w.find_shape(&s_eid).unwrap(), point!(-2, 0, -10));
        assert_eq!(p, point!(0, 0, -1));
    }

    #[test]
    fn convert_normal_from_object_to_world_space() {
        let mut w = World::new();
        let mut g1 = Group::new(&mut w);
        g1.set_transform(rotation_y(PI / 2.));
        let mut g2 = Group::new(&mut w);
        g2.set_transform(scaling(1., 2., 3.));
        g1.add_child(&mut g2);
        let mut s = Sphere::new(&mut w);
        s.set_transform(translation(5., 0., 0.));
        let s_eid = s.eid();
        g2.add_child(&mut s);
        w.add_shape(Shape::Group(g1));
        w.add_shape(Shape::Group(g2));
        w.add_shape(Shape::Sphere(s));
        let n = w.normal_to_world(
            w.find_shape(&s_eid).unwrap(),
            vector!(SQRT_3 / 3., SQRT_3 / 3., SQRT_3 / 3.),
        );
        assert_eq!(n, vector!(0.28571, 0.42857, -0.85714));
    }

    #[test]
    fn normal_of_child_object() {
        let mut w = World::new();
        let mut g1 = Group::new(&mut w);
        g1.set_transform(rotation_y(PI / 2.));
        let mut g2 = Group::new(&mut w);
        g2.set_transform(scaling(1., 2., 3.));
        g1.add_child(&mut g2);
        let mut s = Sphere::new(&mut w);
        s.set_transform(translation(5., 0., 0.));
        let s_eid = s.eid();
        g2.add_child(&mut s);
        w.add_shape(Shape::Group(g1));
        w.add_shape(Shape::Group(g2));
        w.add_shape(Shape::Sphere(s));
        let n = w.find_shape(&s_eid).unwrap().normal_at(
            &point!(1.7321, 1.1547, -5.5774),
            &intersection(0., s_eid),
            &w,
        );
        assert_eq!(n, vector!(0.28570, 0.42854, -0.85716));
    }
}
