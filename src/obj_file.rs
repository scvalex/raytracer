use crate::eid::Eid;
use crate::entity::Entity;
use crate::group::Group;
use crate::shape::Shape;
use crate::triangle::Triangle;
use crate::tuple::{Point, Vector, ORIGIN};
use crate::world::World;
use std::fs::File;
use std::io::{BufRead, BufReader, Read};

pub struct ObjFileContents {
    pub lines: usize,
    pub vertices: Vec<Point>,
    pub normals: Vec<Vector>,
    pub faces: Vec<(
        (usize, Option<usize>, Option<usize>),
        (usize, Option<usize>, Option<usize>),
        (usize, Option<usize>, Option<usize>),
    )>,
}

fn parse_line_as_numbers(l: &str) -> Vec<(f64, Option<f64>, Option<f64>)> {
    let numbers: Vec<(f64, Option<f64>, Option<f64>)> = l
        .trim()
        .split_whitespace()
        .map(|w| {
            let w = w.trim();
            if w.is_empty() {
                None
            } else {
                let xs: Vec<&str> = w.split('/').collect();
                if xs.len() == 1 {
                    xs[0].parse().ok().map(|v| (v, None, None))
                } else {
                    xs[0].parse().ok().map(|v| {
                        (
                            v,
                            xs.get(1).and_then(|x| x.parse().ok()),
                            xs.get(2).and_then(|x| x.parse().ok()),
                        )
                    })
                }
            }
        })
        .filter_map(|x| x)
        .collect();
    numbers
}

fn fan_triangulation<T: Copy>(vertices: Vec<T>) -> Vec<(T, T, T)> {
    let mut triangles = vec![];
    for i in 2..vertices.len() {
        triangles.push((vertices[0], vertices[i - 1], vertices[i]));
    }
    triangles
}

impl ObjFileContents {
    fn parse_bufreader(reader: BufReader<&[u8]>) -> Result<Self, failure::Error> {
        let mut vertices = vec![ORIGIN]; // Always have ORIGIN as vertex 0.
        let mut normals = vec![vector!(0, 0, 0)]; // Normals also start indexed at 1
        let mut faces = vec![];
        let mut lines = 0;
        for l in reader.lines() {
            let l = l.unwrap();
            if l.starts_with("v ") {
                lines += 1;
                let numbers = parse_line_as_numbers(&l);
                vertices.push(point!(numbers[0].0, numbers[1].0, numbers[2].0));
            } else if l.starts_with("vn ") {
                lines += 1;
                let numbers = parse_line_as_numbers(&l);
                normals.push(vector!(numbers[0].0, numbers[1].0, numbers[2].0));
            } else if l.starts_with("f ") {
                lines += 1;
                let vertices = parse_line_as_numbers(&l);
                let mut triangles = fan_triangulation(
                    vertices
                        .into_iter()
                        .map(|(f, t, n)| (f as usize, t.map(|t| t as usize), n.map(|n| n as usize)))
                        .collect(),
                );
                faces.append(&mut triangles);
            }
        }
        Ok(ObjFileContents {
            lines,
            vertices,
            normals,
            faces,
        })
    }

    pub fn parse_string(contents: &str) -> Result<Self, failure::Error> {
        Self::parse_bufreader(BufReader::new(contents.as_bytes()))
    }

    pub fn parse_file(filename: &str) -> Result<Self, failure::Error> {
        let mut file = File::open(filename)?;
        let mut contents = vec![];
        file.read_to_end(&mut contents)?;
        Self::parse_bufreader(BufReader::new(&contents))
    }

    pub fn to_group<'a, 'b>(&'a self, w: &'b mut World) -> Eid {
        let mut g = Group::new(w);
        for (p1, p2, p3) in &self.faces {
            let mut t = match (p1.2, p2.2, p3.2) {
                (Some(n1), Some(n2), Some(n3)) => Triangle::smooth(
                    w,
                    self.vertices[p1.0].clone(),
                    self.vertices[p2.0].clone(),
                    self.vertices[p3.0].clone(),
                    self.normals[n1].clone(),
                    self.normals[n2].clone(),
                    self.normals[n3].clone(),
                ),
                _ => Triangle::new(
                    w,
                    self.vertices[p1.0].clone(),
                    self.vertices[p2.0].clone(),
                    self.vertices[p3.0].clone(),
                ),
            };
            g.add_child(&mut t);
            w.add_shape(Shape::Triangle(t));
        }
        let g_eid = g.eid();
        w.add_shape(Shape::Group(g));
        g_eid
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::shape::ShapeStore;

    #[test]
    fn ignore_unrecognized_lines() {
        let contents = "There was a young lady named Bright
who traveled much faster than light.
She set out one day
in a relative way,
and came back the previous night.
";
        let res = ObjFileContents::parse_string(contents);
        assert_eq!(res.unwrap().lines, 0);
    }

    #[test]
    fn vertex_records() {
        let contents = "v -1 1 0
v -1.0000 0.5000 0.0000
v 1 0 0
v 1 1 0
";
        let res = ObjFileContents::parse_string(contents).unwrap();
        assert_eq!(res.vertices.len(), 5);
        assert_eq!(res.vertices[1], point!(-1, 1, 0));
        assert_eq!(res.vertices[2], point!(-1, 0.5, 0));
        assert_eq!(res.vertices[3], point!(1, 0, 0));
        assert_eq!(res.vertices[4], point!(1, 1, 0));
    }

    #[test]
    fn vertex_records_with_normals_and_texture() {
        let contents = "v -1 1 0
v -1/100 1/100 0/100
v -1/100/200 1/100/200 0/100/200
";
        let res = ObjFileContents::parse_string(contents).unwrap();
        assert_eq!(res.vertices.len(), 4);
        assert_eq!(res.vertices[1], point!(-1, 1, 0));
        assert_eq!(res.vertices[2], point!(-1, 1, 0));
        assert_eq!(res.vertices[3], point!(-1, 1, 0));
    }

    #[test]
    fn triangle_faces() {
        let contents = "v -1 1 0
v -1 0 0
v 1 0 0
v 1 1 0

f 1 2 3
f 1 3 4
";
        let res = ObjFileContents::parse_string(contents).unwrap();
        let mut w = World::new();
        let g = res.to_group(&mut w);
        let g = w.find_shape(&g).unwrap().to_group().unwrap();
        {
            let t1 = w.find_shape(&g.members[0]).unwrap().to_triangle().unwrap();
            assert_eq!(t1.p1, res.vertices[1]);
            assert_eq!(t1.p2, res.vertices[2]);
            assert_eq!(t1.p3, res.vertices[3]);
        }
        {
            let t2 = w.find_shape(&g.members[1]).unwrap().to_triangle().unwrap();
            assert_eq!(t2.p1, res.vertices[1]);
            assert_eq!(t2.p2, res.vertices[3]);
            assert_eq!(t2.p3, res.vertices[4]);
        }
    }

    #[test]
    fn triangulating_polygonds() {
        let contents = "v -1 1 0
v -1 0 0
v 1 0 0
v 1 1 0
v 0 2 0

f 1 2 3 4 5
";
        let res = ObjFileContents::parse_string(contents).unwrap();
        let mut w = World::new();
        let g = res.to_group(&mut w);
        let g = w.find_shape(&g).unwrap().to_group().unwrap();
        {
            let t1 = w.find_shape(&g.members[0]).unwrap().to_triangle().unwrap();
            assert_eq!(t1.p1, res.vertices[1]);
            assert_eq!(t1.p2, res.vertices[2]);
            assert_eq!(t1.p3, res.vertices[3]);
        }
        {
            let t2 = w.find_shape(&g.members[1]).unwrap().to_triangle().unwrap();
            assert_eq!(t2.p1, res.vertices[1]);
            assert_eq!(t2.p2, res.vertices[3]);
            assert_eq!(t2.p3, res.vertices[4]);
        }
        {
            let t2 = w.find_shape(&g.members[2]).unwrap().to_triangle().unwrap();
            assert_eq!(t2.p1, res.vertices[1]);
            assert_eq!(t2.p2, res.vertices[4]);
            assert_eq!(t2.p3, res.vertices[5]);
        }
    }

    #[test]
    fn vertex_normal_records() {
        let contents = "vn 0 0 1
vn 0.707 0 -0.707
vn 1 2 3
";
        let res = ObjFileContents::parse_string(contents).unwrap();
        assert_eq!(res.normals.len(), 4);
        assert_eq!(res.normals[1], vector!(0, 0, 1));
        assert_eq!(res.normals[2], vector!(0.707, 0, -0.707));
        assert_eq!(res.normals[3], vector!(1, 2, 3));
    }
}
