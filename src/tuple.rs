use crate::robust_eq::*;
use std::borrow::Borrow;

#[allow(clippy::approx_constant)]
pub const SQRT_2: f64 = 1.414_213_562_373_095_1;
pub const SQRT_3: f64 = 1.732_050_807_568_877_3;

#[derive(Clone, Copy, Debug)]
pub struct Tuple {
    pub x: f64,
    pub y: f64,
    pub z: f64,
    pub w: f64,
}

// TODO: Make these real newtypes, or maybe use phantom types.
pub type Point = Tuple;
pub type Vector = Tuple;

#[macro_export]
macro_rules! tuple {
    ($x:expr, $y:expr, $z:expr, $w:expr) => {
        Tuple {
            x: (($x) as f64),
            y: (($y) as f64),
            z: (($z) as f64),
            w: (($w) as f64),
        }
    };
}

#[macro_export]
macro_rules! point {
    ($x:expr, $y:expr, $z:expr) => {
        crate::tuple::Tuple {
            x: (($x) as f64),
            y: (($y) as f64),
            z: (($z) as f64),
            w: 1.,
        }
    };
}

#[macro_export]
macro_rules! vector {
    ($x:expr, $y:expr, $z:expr) => {
        crate::tuple::Tuple {
            x: ($x as f64),
            y: ($y as f64),
            z: ($z as f64),
            w: 0.,
        }
    };
}

pub static ORIGIN: Point = point!(0, 0, 0);

impl Tuple {
    pub fn is_point(&self) -> bool {
        float_eq_robust(self.w, 1.0)
    }

    pub fn is_vector(&self) -> bool {
        float_eq_robust(self.w, 0.0)
    }

    pub fn magnitude(&self) -> f64 {
        (self.x * self.x + self.y * self.y + self.z * self.z + self.w * self.w).sqrt()
    }

    pub fn normalize(&self) -> Self {
        let l = self.magnitude();
        tuple!(self.x / l, self.y / l, self.z / l, self.w / l)
    }

    pub fn dot(&self, other: impl Borrow<Self>) -> f64 {
        let other = other.borrow();
        self.x * other.x + self.y * other.y + self.z * other.z + self.w * other.w
    }

    pub fn cross(&self, other: impl Borrow<Self>) -> Self {
        let other = other.borrow();
        vector!(
            self.y * other.z - self.z * other.y,
            self.z * other.x - self.x * other.z,
            self.x * other.y - self.y * other.x
        )
    }
}

impl PartialEq for Tuple {
    fn eq(&self, other: &Self) -> bool {
        float_eq_robust(self.x, other.x)
            && float_eq_robust(self.y, other.y)
            && float_eq_robust(self.z, other.z)
            && float_eq_robust(self.w, other.w)
    }
}

impl std::ops::Add for Tuple {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        assert!(
            float_eq_robust(self.w, 0.0) || float_eq_robust(other.w, 0.0),
            "tried to add two points: {:?} {:?}",
            self,
            other
        );
        tuple!(
            self.x + other.x,
            self.y + other.y,
            self.z + other.z,
            self.w + other.w
        )
    }
}

impl std::ops::Sub for Tuple {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        assert!(!(float_eq_robust(self.w, 0.0) && float_eq_robust(other.w, 1.0)));
        tuple!(
            self.x - other.x,
            self.y - other.y,
            self.z - other.z,
            self.w - other.w
        )
    }
}

impl std::ops::Neg for Tuple {
    type Output = Self;
    fn neg(self) -> Self {
        tuple!(-self.x, -self.y, -self.z, -self.w)
    }
}

impl std::ops::Mul<f64> for Tuple {
    type Output = Self;
    fn mul(self, other: f64) -> Self {
        tuple!(
            self.x * other,
            self.y * other,
            self.z * other,
            self.w * other
        )
    }
}

impl std::ops::Div<f64> for Tuple {
    type Output = Self;
    fn div(self, other: f64) -> Self {
        tuple!(
            self.x / other,
            self.y / other,
            self.z / other,
            self.w / other
        )
    }
}

impl Tuple {
    pub fn reflect(&self, n: &Vector) -> Vector {
        assert!(n.is_vector());
        *self - *n * 2. * self.dot(n)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn tuples() {
        assert_eq!(tuple!(1, 1, 1, 1), tuple!(1, 1, 1, 1));
        assert_ne!(tuple!(-1, 1, 1, 1), tuple!(1, 1, 1, 1));
        assert_eq!(tuple!(1, 1, 1, 1), tuple!(1.000001, 0.999999, 1, 1));
        assert_eq!(point!(4, -4, 3), tuple!(4, -4, 3, 1));
        assert_eq!(vector!(4, -4, 3), tuple!(4, -4, 3, 0));
        assert_eq!(
            tuple!(3, -2, 5, 1) + tuple!(-2, 3, 1, 0),
            tuple!(1, 1, 6, 1)
        );
        assert_eq!(point!(3, 2, 1) - point!(5, 6, 7), vector!(-2, -4, -6));
        assert_eq!(point!(3, 2, 1) - vector!(5, 6, 7), point!(-2, -4, -6));
        assert_eq!(vector!(3, 2, 1) - vector!(5, 6, 7), vector!(-2, -4, -6));
        assert_eq!(-tuple!(1, -2, 3, -4), tuple!(-1, 2, -3, 4));
        assert_eq!(tuple!(1, -2, 3, -4) * 3.5, tuple!(3.5, -7, 10.5, -14));
        assert_eq!(tuple!(1, -2, 3, -4) * 0.5, tuple!(0.5, -1, 1.5, -2));
        assert_eq!(tuple!(1, -2, 3, -4) / 2.0, tuple!(0.5, -1, 1.5, -2));
        assert_eq!(vector!(1, 0, 0).magnitude(), 1.0);
        assert_eq!(vector!(0, 1, 0).magnitude(), 1.0);
        assert_eq!(vector!(0, 0, 1).magnitude(), 1.0);
        assert_eq!(vector!(1, 2, 3).magnitude(), (14.0 as f64).sqrt());
        assert_eq!(vector!(-1, -2, -3).magnitude(), (14.0 as f64).sqrt());
        assert_eq!(vector!(4, 0, 0).normalize(), vector!(1, 0, 0));
        assert_eq!(
            vector!(1, 2, 3).normalize(),
            vector!(0.26726, 0.53452, 0.80178)
        );
        assert_eq!(vector!(1, 2, 3).normalize().magnitude(), 1.0);
        assert_eq!(vector!(1, 2, 3).dot(vector!(2, 3, 4)), 20.0);
        assert_eq!(vector!(1, 2, 3).cross(vector!(2, 3, 4)), vector!(-1, 2, -1));
        assert_eq!(vector!(2, 3, 4).cross(vector!(1, 2, 3)), vector!(1, -2, 1));
        assert_eq!(vector!(1, 0, 0).cross(vector!(0, 1, 0)), vector!(0, 0, 1));
        assert_eq!(vector!(0, 1, 0).cross(vector!(0, 0, 1)), vector!(1, 0, 0));
        assert_eq!(vector!(0, 0, 1).cross(vector!(1, 0, 0)), vector!(0, 1, 0));
    }

    #[test]
    fn reflect_45() {
        let v = vector!(1, -1, 0);
        let n = vector!(0, 1, 0);
        let r = v.reflect(&n);
        assert_eq!(r, vector!(1, 1, 0));
    }

    #[test]
    fn reflect_90() {
        let v = vector!(0, -1, 0);
        let n = vector!(SQRT_2 / 2., SQRT_2 / 2., 0);
        let r = v.reflect(&n);
        assert_eq!(r, vector!(1, 0, 0));
    }
}
