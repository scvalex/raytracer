use failure::bail;
use raytracer::*;

fn main() -> Result<(), failure::Error> {
    let args: Vec<String> = std::env::args().collect();
    let args: Vec<&str> = args.iter().map(|str| str.as_ref()).collect();
    match args.as_slice() {
        [_, "gradient", file] => {
            let mut c = canvas(100, 200);
            for i in 0..100 {
                for j in 0..200 {
                    c.write_pixel(i, j, BLUE * (j as f64 / 200.0));
                }
            }
            c.write_to_file(file)
        }
        [_, "clock", file] => {
            const SIZE: usize = 200;
            const SIZE_F: f64 = SIZE as f64;
            let mut c = canvas(SIZE, SIZE);
            let p = point!(0, 1, 0);
            for i in 0..SIZE {
                c.write_pixel(i, 0, WHITE);
                c.write_pixel(0, i, WHITE);
                c.write_pixel(i, SIZE - 1, WHITE);
                c.write_pixel(SIZE - 1, i, WHITE);
            }
            for i in 0..12 {
                let p2 = p
                    .scale(SIZE_F / 2. * 0.75, SIZE_F / 2. * 0.75, 1.)
                    .rotate_z(PI / 6. * (i as f64))
                    .translate(SIZE_F / 2., SIZE_F / 2., 0.);
                println!("{:?}", p2);
                c.write_pixel(p2.x as usize, p2.y as usize, WHITE);
            }
            c.write_to_file(file)
        }
        [_, "spheres", file] => {
            let mut world = World::new();
            world.light = point_light(point!(-10, 10, -10), WHITE);
            {
                let floor = world.add_plane();
                floor.set_material({
                    let mut m = material();
                    m.color = color!(0, 0.9, 0);
                    m.specular = 0.;
                    m.diffuse = 0.9;
                    m.ambient = 0.1;
                    let mut pattern1 = StripePattern::new(BLACK, WHITE);
                    pattern1.set_transform(rotation_y(PI / 4. * 3.) * scaling(0.1, 0.1, 0.1));
                    let mut pattern2 = StripePattern::new(color!(0.5, 0.5, 0), WHITE);
                    pattern2.set_transform(rotation_y(PI / 4.) * scaling(0.3, 0.3, 0.3));
                    m.pattern = Some(std::rc::Rc::new(BlendedPattern::new(pattern1, pattern2)));
                    m
                });
            }
            {
                let middle = world.add_sphere();
                middle.set_transform(translation(0., 1., 0.5));
                middle.set_material({
                    let mut m = material();
                    m.diffuse = 0.;
                    m.specular = 0.2;
                    m.ambient = 0.1;
                    m.reflective = 1.;
                    m
                });
            }
            {
                let right = world.add_sphere();
                right.set_transform(translation(2.5, 2.5, 0.) * scaling(0.5, 0.5, 0.5));
                right.set_material({
                    let mut m = material();
                    m.color = BLUE;
                    m.diffuse = 0.7;
                    m.specular = 0.3;
                    let mut pattern = RingPattern::new(BLUE, YELLOW);
                    pattern.set_transform(
                        scaling(0.1, 0.1, 0.1) * rotation_y(PI / 10. * 2.) * rotation_x(PI / 2.),
                    );
                    m.pattern = Some(std::rc::Rc::new(pattern));
                    m
                });
            }
            {
                let left = world.add_sphere();
                left.set_transform(translation(-1.5, 1. / 6., -1.5) * scaling(0.33, 0.66, 0.33));
                left.set_material({
                    let mut m = material();
                    m.color = RED;
                    m.diffuse = 0.7;
                    m.specular = 0.3;
                    let mut pattern = CheckersPattern::new(YELLOW, RED);
                    pattern.set_transform(scaling(0.2, 0.2, 0.2));
                    m.pattern = Some(std::rc::Rc::new(pattern));
                    m
                });
            }
            {
                let front = world.add_sphere();
                front.set_transform(translation(-0.75, 1.0, -0.75) * scaling(0.33, 0.33, 0.33));
                let m = front.material_mut();
                m.ambient = 0.;
                m.specular = 0.;
                m.diffuse = 0.;
                m.casts_shadow = false;
                m.transparency = 1.;
                m.reflective = 0.;
                m.refractive_index = REFRACTIVE_GLASS;
            }
            {
                let behind = world.add_sphere();
                behind.set_transform(scaling(3., 3., 3.) * translation(3., 1., -6.));
                let m = behind.material_mut();
                m.color = WHITE;
            }
            {
                let water = world.add_plane();
                water.set_transform(translation(0., 0.6, 0.));
                let mut m = water.material_mut();
                m.color = color!(0.4, 0.4, 1);
                m.shininess = 1.;
                m.specular = 0.;
                m.ambient = 0.;
                m.diffuse = 0.4;
                m.casts_shadow = false;
                m.reflective = 0.8;
                m.transparency = 0.9;
                m.refractive_index = REFRACTIVE_WATER;
            }
            let mut camera = Camera::new(600, 400, PI / 3.);
            camera.transform =
                view_transform(point!(0, 1.5, -5), point!(0, 1, 0), vector!(0, 1, 0));
            let canvas = camera.render(&world);
            canvas.write_to_file(file)
        }
        [_, "test", file] => {
            let mut world = World::new();
            world.light = point_light(point!(-10, 10, -10), WHITE);
            {
                let floor = world.add_plane();
                floor.set_material({
                    let mut m = material();
                    m.color = color!(0, 0.9, 0);
                    m.specular = 0.;
                    m.diffuse = 0.9;
                    m.ambient = 0.1;
                    let mut pattern1 = StripePattern::new(BLACK, WHITE);
                    pattern1.set_transform(rotation_y(PI / 4. * 3.) * scaling(0.1, 0.1, 0.1));
                    let mut pattern2 = StripePattern::new(color!(0.5, 0.5, 0), WHITE);
                    pattern2.set_transform(rotation_y(PI / 4.) * scaling(0.3, 0.3, 0.3));
                    m.pattern = Some(std::rc::Rc::new(BlendedPattern::new(pattern1, pattern2)));
                    m
                });
            }
            {
                let sky = world.add_plane();
                sky.material_mut().color = BLUE;
                sky.set_transform(translation(0., 20., 0.));
            }
            {
                for i in 0..3 {
                    let c = world.add_cube();
                    let i = i as f64;
                    c.set_transform(
                        rotation_y(PI / 3. + i * (PI / 6.))
                            * scaling(0.5 - i * 0.2, 0.5, 0.5 - i * 0.2)
                            * translation(0., 1. + 2. * i, 0.),
                    );
                    c.material_mut().color = RED;
                }
            }
            {
                let mut c = world.add_cone();
                c.maximum = 0.;
                c.minimum = -1.;
                c.closed = true;
                c.set_transform(scaling(1., 2., 1.) * translation(1., 1., 1.));
                c.material_mut().reflective = 1.;
                c.material_mut().diffuse = 0.;
            }
            {
                let mut c = world.add_cylinder();
                c.maximum = 0.5;
                c.closed = true;
                c.set_transform(scaling(1., 1., 1.) * translation(0.9, 0.0, -1.7));
                c.material_mut().reflective = 1.;
                c.material_mut().diffuse = 0.;
            }
            {
                let mut hex = Group::new(&mut world);
                for i in 0..6 {
                    let mut corner = Sphere::new(&mut world);
                    corner.set_transform(translation(0., 0., -1.) * scaling(0.25, 0.25, 0.25));
                    let mut edge = Cylinder::new(&mut world);
                    edge.minimum = 0.;
                    edge.maximum = 1.;
                    edge.set_transform(
                        translation(0., 0., -1.)
                            * rotation_y(-PI / 6.)
                            * rotation_z(-PI / 2.)
                            * scaling(0.25, 1., 0.25),
                    );
                    let mut group = Group::new(&mut world);
                    let mut corner = Shape::Sphere(corner);
                    let mut edge = Shape::Cylinder(edge);
                    group.add_child(&mut corner);
                    group.add_child(&mut edge);
                    group.set_transform(rotation_y((i as f64) * PI / 3.));
                    hex.add_child(&mut group);
                    world.add_shape(corner);
                    world.add_shape(edge);
                    world.add_shape(Shape::Group(group));
                }
                hex.set_transform(translation(-3., 0., 0.));
                world.add_shape(Shape::Group(hex));
            }
            let mut camera = Camera::new(600, 400, PI / 3.);
            camera.transform = view_transform(point!(0, 5, -5), point!(0, 1, 0), vector!(0, 1, 0));
            let canvas = camera.render(&world);
            canvas.write_to_file(file)
        }
        [_, "obj", input, file] => {
            let mut world = World::new();
            world.light = point_light(point!(-10, 10, -10), WHITE);
            {
                let floor = world.add_plane();
                floor.material_mut().pattern =
                    Some(std::rc::Rc::new(CheckersPattern::new(BLACK, WHITE)));
            }
            {
                let sky = world.add_plane();
                sky.material_mut().color = BLUE;
                sky.set_transform(translation(0., 20., 0.));
            }
            {
                let obj_file = ObjFileContents::parse_file(input)?;
                let g_eid = obj_file.to_group(&mut world);
                world.find_shape_mut(&g_eid).unwrap().set_transform(
                    translation(0., 1., -1.) * scaling(0.1, 0.1, 0.1) * rotation_x(-PI / 2.),
                );
            }
            let mut camera = Camera::new(600, 400, PI / 3.);
            camera.transform = view_transform(point!(0, 5, -5), point!(0, 1, 0), vector!(0, 1, 0));
            let canvas = camera.render(&world);
            canvas.write_to_file(file)
        }
        [_, "--version"] => {
            println!("{}", env!("CARGO_PKG_VERSION"));
            Ok(())
        }
        _ => bail!("not enough arguments"),
    }
}
