pub fn float_eq_robust(x: f64, y: f64) -> bool {
    (x - y).abs() < 1e-5
}
