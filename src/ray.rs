use crate::matrix::*;
use crate::tuple::*;

// TODO: Get rid of this Copy.
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Ray {
    pub origin: Point,
    pub direction: Vector,
}

impl Ray {
    pub fn new(origin: Point, direction: Vector) -> Self {
        assert!(origin.is_point());
        assert!(direction.is_vector());
        Ray { origin, direction }
    }

    pub fn position(&self, t: f64) -> Point {
        self.origin + self.direction * t
    }

    pub fn transform(&self, m: Matrix4) -> Ray {
        Ray {
            origin: m * self.origin,
            direction: m * self.direction,
        }
    }
}

pub fn ray(origin: Point, direction: Vector) -> Ray {
    Ray::new(origin, direction)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::transform::*;

    #[test]
    fn ray_accessors() {
        let origin = point!(1, 2, 3);
        let direction = vector!(4, 5, 6);
        let r = ray(origin, direction);
        assert_eq!(r.origin, origin);
        assert_eq!(r.direction, direction);
    }

    #[test]
    fn distance() {
        let r = ray(point!(2, 3, 4), vector!(1, 0, 0));
        assert_eq!(r.position(0.), point!(2, 3, 4));
        assert_eq!(r.position(1.), point!(3, 3, 4));
        assert_eq!(r.position(-1.), point!(1, 3, 4));
        assert_eq!(r.position(2.5), point!(4.5, 3, 4));
    }

    #[test]
    fn translate() {
        let r = ray(point!(1, 2, 3), vector!(0, 1, 0));
        let m = translation(3., 4., 5.);
        let r2 = r.transform(m);
        assert_eq!(r2.origin, point!(4, 6, 8));
        assert_eq!(r2.direction, vector!(0, 1, 0));
    }

    #[test]
    fn scale() {
        let r = ray(point!(1, 2, 3), vector!(0, 1, 0));
        let m = scaling(2., 3., 4.);
        let r2 = r.transform(m);
        assert_eq!(r2.origin, point!(2, 6, 12));
        assert_eq!(r2.direction, vector!(0, 3, 0));
    }
}
