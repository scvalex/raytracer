#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Eid(usize);

pub trait EidAllocator {
    /// Alocates a fresh non-repeating `Eid`.
    ///
    /// The idea behind the `new_eid` and `new_eid_usize` functions is that,
    /// together, they hide the constructor of `Eid`: The constructor is private
    /// and can only be called from this module, so the only function that ever
    /// creates `Eid`s is `new_eid`. Implementing types provide `new_eid_usize`
    /// which is wrapped automatically in `Eid` by `new_eid`.
    fn new_eid(&mut self) -> Eid {
        Eid(self.new_eid_usize())
    }

    fn new_eid_usize(&mut self) -> usize;
}
