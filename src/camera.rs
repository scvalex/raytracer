use crate::canvas::*;
use crate::matrix::*;
use crate::ray::*;
use crate::tuple::*;
use crate::world::World;

pub struct Camera {
    hsize: usize,
    vsize: usize,
    #[allow(dead_code)]
    field_of_view: f64,
    pub transform: Matrix4,
    pixel_size: f64,
    half_width: f64,
    half_height: f64,
}

impl Camera {
    pub fn new(hsize: usize, vsize: usize, field_of_view: f64) -> Self {
        let half_view = (field_of_view / 2.).tan();
        let aspect = (hsize as f64) / (vsize as f64);
        let (half_width, half_height) = if aspect >= 1. {
            (half_view, half_view / aspect)
        } else {
            (half_view * aspect, half_view)
        };
        Camera {
            hsize,
            vsize,
            field_of_view,
            transform: M4_ID,
            pixel_size: half_width * 2. / (hsize as f64),
            half_width,
            half_height,
        }
    }

    pub fn ray_for_pixel(&self, px: usize, py: usize) -> Ray {
        let xoffset = ((px as f64) + 0.5) * self.pixel_size;
        let yoffset = ((py as f64) + 0.5) * self.pixel_size;
        let world_x = self.half_width - xoffset;
        let world_y = self.half_height - yoffset;
        let inv = self.transform.inverse();
        let pixel = inv * point!(world_x, world_y, -1);
        let origin = inv * ORIGIN;
        let direction = (pixel - origin).normalize();
        ray(origin, direction)
    }

    pub fn render(&self, world: &World) -> Canvas {
        let mut image = canvas(self.hsize, self.vsize);
        for y in 0..self.vsize {
            print!("{}/{}: ", y, self.vsize);
            for x in 0..self.hsize {
                let ray = self.ray_for_pixel(x, y);
                let color = world.color_at(&ray, 5);
                image.write_pixel(x, self.vsize - y - 1, color);
                if x % (1 + self.hsize / 40) == 0 {
                    print!(".");
                }
            }
            println!("");
        }
        image
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::color::*;
    use crate::robust_eq::*;
    use crate::transform::*;
    use std::f64::consts::PI;

    #[test]
    fn default_camera() {
        let c = Camera::new(160, 120, PI);
        assert_eq!(c.hsize, 160);
        assert_eq!(c.vsize, 120);
        assert_eq!(c.field_of_view, PI);
        assert_eq!(c.transform, M4_ID);
    }

    #[test]
    fn pixel_size_horizontal_canvas() {
        let c = Camera::new(200, 125, PI / 2.);
        assert!(float_eq_robust(c.pixel_size, 0.01));
    }

    #[test]
    fn pixel_size_vertical_canvas() {
        let c = Camera::new(125, 200, PI / 2.);
        assert!(float_eq_robust(c.pixel_size, 0.01));
    }

    #[test]
    fn ray_center_of_camera() {
        let c = Camera::new(201, 101, PI / 2.);
        let r = c.ray_for_pixel(100, 50);
        assert_eq!(r.origin, ORIGIN);
        assert_eq!(r.direction, vector!(0, 0, -1));
    }

    #[test]
    fn ray_corner_canvas() {
        let c = Camera::new(201, 101, PI / 2.);
        let r = c.ray_for_pixel(0, 0);
        assert_eq!(r.origin, ORIGIN);
        assert_eq!(r.direction, vector!(0.66519, 0.33259, -0.66851));
    }

    #[test]
    fn ray_transformed_camera() {
        let mut c = Camera::new(201, 101, PI / 2.);
        c.transform = rotation_y(PI / 4.) * translation(0., -2., 5.);
        let r = c.ray_for_pixel(100, 50);
        assert_eq!(r.origin, point!(0, 2, -5));
        assert_eq!(r.direction, vector!(SQRT_2 / 2., 0, -SQRT_2 / 2.));
    }

    #[test]
    fn render_test() {
        let w = World::sample();
        let mut c = Camera::new(11, 11, PI / 2.);
        let from = point!(0, 0, -5);
        let to = point!(0, 0, 0);
        let up = vector!(0, 1, 0);
        c.transform = view_transform(from, to, up);
        let image = c.render(&w);
        assert_eq!(image.pixel_at(5, 5), color!(0.38066, 0.47583, 0.2855));
    }
}
