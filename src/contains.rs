use crate::eid::*;
use crate::shape::ShapeStore;

pub trait HasContains {
    fn contains(&self, eid: &Eid, w: &dyn ShapeStore) -> bool;
}
