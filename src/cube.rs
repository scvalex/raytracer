use crate::contains::*;
use crate::eid::*;
use crate::intersect::*;
use crate::material::*;
use crate::matrix::*;
use crate::normal::*;
use crate::ray::*;
use crate::shape::ShapeStore;
use crate::tuple::*;

#[derive(Clone, Debug)]
pub struct Cube {
    eid: Eid,
    transform: Matrix4,
    material: Material,
    parent: Option<Eid>,
}

shape_boilerplate!(Cube);

impl HasContains for Cube {
    fn contains(&self, eid: &Eid, _: &dyn ShapeStore) -> bool {
        self.eid == *eid
    }
}

impl Cube {
    pub fn new(w: &mut dyn EidAllocator) -> Self {
        Cube {
            eid: w.new_eid(),
            transform: M4_ID,
            material: Material::default(),
            parent: None,
        }
    }
}

fn check_axis(origin: f64, direction: f64) -> (f64, f64) {
    let tmin_numerator = -1. - origin;
    let tmax_numerator = 1. - origin;
    let (mut tmin, mut tmax) = if direction.abs() >= std::f64::EPSILON {
        (tmin_numerator / direction, tmax_numerator / direction)
    } else {
        (
            tmin_numerator * std::f64::INFINITY,
            tmax_numerator * std::f64::INFINITY,
        )
    };
    if tmin > tmax {
        std::mem::swap(&mut tmin, &mut tmax)
    }
    (tmin, tmax)
}

impl HasIntersect for Cube {
    fn local_intersect(&self, r: &Ray, _: &dyn ShapeStore) -> Vec<Intersection> {
        let (xtmin, xtmax) = check_axis(r.origin.x, r.direction.x);
        let (ytmin, ytmax) = check_axis(r.origin.y, r.direction.y);
        let (ztmin, ztmax) = check_axis(r.origin.z, r.direction.z);
        let tmin = f64::max(f64::max(xtmin, ytmin), ztmin);
        let tmax = f64::min(f64::min(xtmax, ytmax), ztmax);
        if tmin > tmax {
            vec![]
        } else {
            vec![intersection(tmin, self.eid), intersection(tmax, self.eid)]
        }
    }
}

impl HasNormal for Cube {
    #[allow(clippy::float_cmp)]
    fn local_normal_at(&self, p: &Point, _: &Intersection, _: &dyn ShapeStore) -> Vector {
        let maxc = f64::max(f64::max(p.x.abs(), p.y.abs()), p.z.abs());

        if maxc == p.x.abs() {
            vector!(p.x, 0, 0)
        } else if maxc == p.y.abs() {
            vector!(0, p.y, 0)
        } else {
            vector!(0, 0, p.z)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::world::World;

    #[test]
    fn ray_intersects_cube() {
        let mut w = World::new();
        let c = Cube::new(&mut w);
        let cases = [
            ("+x", point!(5, 0.5, 0), vector!(-1, 0, 0), 4., 6.),
            ("-x", point!(-5, 0.5, 0), vector!(1, 0, 0), 4., 6.),
            ("+y", point!(0.5, 5, 0), vector!(0, -1, 0), 4., 6.),
            ("-y", point!(0.5, -5, 0), vector!(0, 1, 0), 4., 6.),
            ("+z", point!(0.5, 0, 5), vector!(0, 0, -1), 4., 6.),
            ("-z", point!(0.5, 0, -5), vector!(0, 0, 1), 4., 6.),
            ("inside", point!(0, 0.5, 0), vector!(0, 0, 1), -1., 1.),
        ];
        for (name, origin, direction, t1, t2) in &cases {
            let r = ray(*origin, *direction);
            let xs = c.local_intersect(&r, &w);
            assert_eq!(xs.len(), 2, "{}", name);
            assert_eq!(xs[0].t, *t1, "{}", name);
            assert_eq!(xs[1].t, *t2, "{}", name);
        }
    }

    #[test]
    fn ray_misses_cube() {
        let mut w = World::new();
        let c = Cube::new(&mut w);
        let cases = [
            (point!(-2, 0, 0), vector!(0.2673, 0.5345, 0.8018)),
            (point!(0, -2, 0), vector!(0.8018, 0.2673, 0.5345)),
            (point!(0, 0, -2), vector!(0.5345, 0.8018, 0.2673)),
            (point!(2, 0, 2), vector!(0, 0, -1)),
            (point!(0, 2, 2), vector!(0, -1, 0)),
            (point!(2, 2, 0), vector!(-1, 0, 0)),
        ];
        for (i, (origin, direction)) in cases.iter().enumerate() {
            let r = ray(*origin, *direction);
            let xs = c.local_intersect(&r, &w);
            assert_eq!(xs.len(), 0, "Test #{}", i);
        }
    }

    #[test]
    fn normal_on_surface() {
        let mut w = World::new();
        let c = Cube::new(&mut w);
        let cases = [
            (point!(1, 0.5, -0.8), vector!(1, 0, 0)),
            (point!(-1, -0.2, 0.9), vector!(-1, 0, 0)),
            (point!(-0.4, 1, -0.1), vector!(0, 1, 0)),
            (point!(0.3, -1, -0.7), vector!(0, -1, 0)),
            (point!(-0.6, 0.3, 1), vector!(0, 0, 1)),
            (point!(0.4, 0.4, -1), vector!(0, 0, -1)),
            (point!(1, 1, 1), vector!(1, 0, 0)),
            (point!(-1, -1, -1), vector!(-1, 0, 0)),
        ];
        for (i, (point, expected_normal)) in cases.iter().enumerate() {
            let normal = c.local_normal_at(
                point,
                &intersection((*point - ORIGIN).magnitude(), c.eid),
                &w,
            );
            assert_eq!(normal, *expected_normal, "Test #{}", i);
        }
    }
}
