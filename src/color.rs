use crate::tuple::*;

use std::ops::{Add, Mul, Sub};

macro_rules! obvious_impl {
    (impl $trait_: ident for $type_: ident { fn $method: ident }) => {
        impl $trait_<$type_> for $type_ {
            type Output = $type_;

            fn $method(self, $type_(b): $type_) -> $type_ {
                let $type_(a) = self;
                $type_(a.$method(b))
            }
        }
    };
}

#[derive(Clone, Copy, Debug)]
pub struct Color(pub Tuple);

#[macro_export]
macro_rules! color {
    ($x:expr, $y:expr, $z:expr) => {
        Color(Tuple {
            x: (($x) as f64),
            y: (($y) as f64),
            z: (($z) as f64),
            w: 0.,
        })
    };
}

pub static RED: Color = color!(1, 0, 0);
pub static GREEN: Color = color!(0, 1, 0);
pub static BLUE: Color = color!(0, 0, 1);
pub static WHITE: Color = color!(1, 1, 1);
pub static BLACK: Color = color!(0, 0, 0);
pub static YELLOW: Color = color!(1, 1, 0);

fn clamp_to_8bit(x: f64) -> u8 {
    let mut x = x;
    if x < 0.0 {
        x = 0.0;
    }
    if x > 1.0 {
        x = 1.0;
    }
    (x * 255.0) as u8
}

impl Color {
    pub fn red(&self) -> f64 {
        let Color(c) = self;
        c.x
    }

    pub fn green(&self) -> f64 {
        let Color(c) = self;
        c.y
    }

    pub fn blue(&self) -> f64 {
        let Color(c) = self;
        c.z
    }

    pub fn to_8bit(&self) -> (u8, u8, u8) {
        (
            clamp_to_8bit(self.red()),
            clamp_to_8bit(self.green()),
            clamp_to_8bit(self.blue()),
        )
    }
}

impl PartialEq for Color {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

obvious_impl!(impl Add for Color { fn add });
obvious_impl!(impl Sub for Color { fn sub });

impl Mul<f64> for Color {
    type Output = Self;
    fn mul(self, other: f64) -> Self {
        Color(self.0 * other)
    }
}

impl Mul<Color> for Color {
    type Output = Self;
    fn mul(self, other: Color) -> Self {
        color!(
            self.red() * other.red(),
            self.green() * other.green(),
            self.blue() * other.blue()
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn colors() {
        assert_eq!(
            color!(0.9, 0.6, 0.75) + color!(0.7, 0.1, 0.25),
            color!(1.6, 0.7, 1)
        );
        assert_eq!(
            color!(0.9, 0.6, 0.75) - color!(0.7, 0.1, 0.25),
            color!(0.2, 0.5, 0.5)
        );
        assert_eq!(color!(0.2, 0.3, 0.4) * 2.0, color!(0.4, 0.6, 0.8));
        assert_eq!(
            color!(1, 0.2, 0.4) * color!(0.9, 1, 0.1),
            color!(0.9, 0.2, 0.04)
        );
    }
}
