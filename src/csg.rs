use crate::contains::HasContains;
use crate::eid::*;
use crate::intersect::*;
use crate::material::*;
use crate::matrix::*;
use crate::normal::*;
use crate::parent::HasParent;
use crate::ray::*;
use crate::shape::ShapeStore;
use crate::tuple::*;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Operation {
    Union,
    Intersection,
    Difference,
}

#[derive(Clone, Debug)]
pub struct Csg {
    eid: Eid,
    transform: Matrix4,
    material: Material,
    parent: Option<Eid>,
    left: Eid,
    right: Eid,
    operation: Operation,
}

shape_boilerplate!(Csg);

impl HasContains for Csg {
    fn contains(&self, eid: &Eid, w: &dyn ShapeStore) -> bool {
        self.eid == *eid
            || self.left == *eid
            || self.right == *eid
            || w.find_shape(&self.left).unwrap().contains(eid, w)
            || w.find_shape(&self.right).unwrap().contains(eid, w)
    }
}

impl Csg {
    pub fn new(w: &mut dyn ShapeStore, left: Eid, right: Eid, operation: Operation) -> Self {
        let eid = w.new_eid();
        w.find_shape_mut(&left).unwrap().set_parent(&eid);
        w.find_shape_mut(&right).unwrap().set_parent(&eid);
        Csg {
            eid,
            transform: M4_ID,
            material: Material::default(),
            parent: None,
            left,
            right,
            operation,
        }
    }

    pub fn filter_intersections(
        &self,
        xs: Vec<Intersection>,
        w: &dyn ShapeStore,
    ) -> Vec<Intersection> {
        let mut inl = false;
        let mut inr = false;
        let mut result = vec![];
        for i in xs {
            let lhit = w.find_shape(&self.left).unwrap().contains(&i.object, w);
            if intersection_allowed(&self.operation, lhit, inl, inr) {
                result.push(i);
            }
            if lhit {
                inl = !inl;
            } else {
                inr = !inr;
            }
        }
        result
    }
}

pub fn intersection_allowed(operation: &Operation, lhit: bool, inl: bool, inr: bool) -> bool {
    match operation {
        Operation::Union => (lhit && !inr) || (!lhit && !inl),
        Operation::Intersection => (lhit && inr) || (!lhit && inl),
        Operation::Difference => (lhit && !inr) || (!lhit && inl),
    }
}

impl HasIntersect for Csg {
    fn local_intersect(&self, r: &Ray, w: &dyn ShapeStore) -> Vec<Intersection> {
        let mut xs = w.find_shape(&self.left).unwrap().intersect(r, w);
        xs.append(&mut w.find_shape(&self.right).unwrap().intersect(r, w));
        xs.sort();
        self.filter_intersections(xs, w)
    }
}

impl HasNormal for Csg {
    fn local_normal_at(&self, _: &Point, _: &Intersection, _: &dyn ShapeStore) -> Vector {
        vector!(0, 0, 0)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::entity::Entity;
    use crate::transform::*;
    use crate::world::World;

    #[test]
    fn creating_csg() {
        let mut w = World::new();
        let s1 = {
            let s = w.add_sphere();
            s.eid()
        };
        let s2 = {
            let s = w.add_cube();
            s.eid()
        };
        let c = {
            let s = w.add_csg(s1, s2, Operation::Union);
            s.eid()
        };
        let c = w.find_shape(&c).unwrap().to_csg().unwrap();
        assert_eq!(c.operation, Operation::Union);
        assert_eq!(c.left, s1);
        assert_eq!(c.right, s2);
        assert_eq!(w.find_shape(&s1).unwrap().parent(), Some(c.eid()));
        assert_eq!(w.find_shape(&s2).unwrap().parent(), Some(c.eid()));
    }

    #[test]
    fn evaluate_operation() {
        let cases = [
            (Operation::Union, true, true, true, false),
            (Operation::Union, true, true, false, true),
            (Operation::Union, true, false, true, false),
            (Operation::Union, true, false, false, true),
            (Operation::Union, false, true, true, false),
            (Operation::Union, false, true, false, false),
            (Operation::Union, false, false, true, true),
            (Operation::Union, false, false, false, true),
            (Operation::Intersection, true, true, true, true),
            (Operation::Intersection, true, true, false, false),
            (Operation::Intersection, true, false, true, true),
            (Operation::Intersection, true, false, false, false),
            (Operation::Intersection, false, true, true, true),
            (Operation::Intersection, false, true, false, true),
            (Operation::Intersection, false, false, true, false),
            (Operation::Intersection, false, false, false, false),
            (Operation::Difference, true, true, true, false),
            (Operation::Difference, true, true, false, true),
            (Operation::Difference, true, false, true, false),
            (Operation::Difference, true, false, false, true),
            (Operation::Difference, false, true, true, true),
            (Operation::Difference, false, true, false, true),
            (Operation::Difference, false, false, true, false),
            (Operation::Difference, false, false, false, false),
        ];
        for (idx, (op, lhit, inl, inr, result)) in cases.iter().enumerate() {
            assert_eq!(
                intersection_allowed(op, *lhit, *inl, *inr),
                *result,
                "Test {}",
                idx
            );
        }
    }

    #[test]
    fn filtering_intersections() {
        let cases = [
            (Operation::Union, 0, 3),
            (Operation::Intersection, 1, 2),
            (Operation::Difference, 0, 1),
        ];
        for (idx, (operation, x0, x1)) in cases.iter().enumerate() {
            let mut w = World::new();
            let s1 = {
                let s = w.add_sphere();
                s.eid()
            };
            let s2 = {
                let s = w.add_cube();
                s.eid()
            };
            let c = {
                let s = w.add_csg(s1, s2, *operation);
                s.eid()
            };
            let c = w.find_shape(&c).unwrap().to_csg().unwrap();
            let xs = vec![
                intersection(1., s1),
                intersection(2., s2),
                intersection(3., s1),
                intersection(4., s2),
            ];
            let result = c.filter_intersections(xs.clone(), &w);
            assert_eq!(result.len(), 2, "Test #{}", idx);
            assert_eq!(result[0], xs[*x0], "Test #{}", idx);
            assert_eq!(result[1], xs[*x1], "Test #{}", idx);
        }
    }

    #[test]
    fn ray_misses_csg() {
        let mut w = World::new();
        let s1 = {
            let s = w.add_sphere();
            s.eid()
        };
        let s2 = {
            let s = w.add_cube();
            s.eid()
        };
        let c = {
            let s = w.add_csg(s1, s2, Operation::Union);
            s.eid()
        };
        let c = w.find_shape(&c).unwrap().to_csg().unwrap();
        let r = ray(point!(0, 2, -5), vector!(0, 0, 1));
        let xs = c.local_intersect(&r, &w);
        assert_eq!(xs.len(), 0);
    }

    #[test]
    fn ray_hits_csg() {
        let mut w = World::new();
        let s1 = {
            let s = w.add_sphere();
            s.eid()
        };
        let s2 = {
            let s = w.add_sphere();
            s.set_transform(translation(0., 0., 0.5));
            s.eid()
        };
        let c = {
            let c = w.add_csg(s1, s2, Operation::Union);
            c.eid()
        };
        let r = ray(point!(0, 0, -5), vector!(0, 0, 1));
        let xs = w.find_shape(&c).unwrap().local_intersect(&r, &w);
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].t, 4.);
        assert_eq!(xs[0].object, s1);
        assert_eq!(xs[1].t, 6.5);
        assert_eq!(xs[1].object, s2);
    }
}
