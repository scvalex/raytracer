use crate::intersect::Intersection;
use crate::shape::ShapeStore;
use crate::transform::*;
use crate::tuple::*;

pub trait HasNormal: HasTransform {
    /// Returns the normal on the shape at the given point. The point is always
    /// given in space.
    fn local_normal_at(&self, p: &Point, i: &Intersection, q: &dyn ShapeStore) -> Vector;
}
