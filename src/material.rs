use crate::color::*;
use crate::light::*;
use crate::pattern::*;
use crate::robust_eq::*;
use crate::shape::{Shape, ShapeStore};
use crate::tuple::*;
use std::rc::Rc;

pub const REFRACTIVE_VACUUM: f64 = 1.;
pub const REFRACTIVE_AIR: f64 = 1.00029;
pub const REFRACTIVE_WATER: f64 = 1.333;
pub const REFRACTIVE_GLASS: f64 = 1.52;
pub const REFRACTIVE_DIAMOND: f64 = 2.417;

#[derive(Clone)]
pub struct Material {
    pub color: Color,
    pub ambient: f64,
    pub diffuse: f64,
    pub specular: f64,
    pub shininess: f64,
    pub pattern: Option<Rc<dyn Pattern>>,
    pub reflective: f64,
    pub transparency: f64,
    pub refractive_index: f64,
    pub casts_shadow: bool,
}

impl std::fmt::Debug for Material {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Material {{ color: {:?}, ambient: {}, diffuse: {}, specular: {}, shininess: {}, pattern: {}}}", self.color, self.ambient, self.diffuse, self.specular, self.shininess, (match self.pattern {
            None => "None",
            Some(_) => "Some(_)",
        }))
    }
}

impl Material {
    pub fn new() -> Self {
        Material {
            color: WHITE,
            ambient: 0.1,
            diffuse: 0.9,
            specular: 0.9,
            shininess: 200.,
            pattern: None,
            reflective: 0.,
            transparency: 0.,
            refractive_index: REFRACTIVE_VACUUM,
            casts_shadow: true,
        }
    }

    pub fn new_glass() -> Self {
        let mut m = Self::new();
        m.transparency = 1.;
        m.refractive_index = REFRACTIVE_GLASS;
        m
    }

    pub fn lighting(
        &self,
        shape: &Shape,
        light: &Light,
        point: &Point,
        eyev: &Vector,
        normalv: &Vector,
        in_shadow: bool,
        shape_store: &dyn ShapeStore,
    ) -> Color {
        let color_used = match &self.pattern {
            None => self.color,
            Some(pattern) => pattern.as_ref().pattern_at_shape(shape, point, shape_store),
        };
        let effective_color = color_used * *light.intensity();
        let lightv = (*light.position() - *point).normalize();
        let ambient = effective_color * self.ambient;
        let light_dot_normal = lightv.dot(normalv);
        let (diffuse, specular) = if in_shadow {
            (BLACK, BLACK)
        } else {
            let diffuse;
            let specular;
            if light_dot_normal < 0. {
                diffuse = BLACK;
                specular = BLACK;
            } else {
                diffuse = effective_color * self.diffuse * light_dot_normal;
                let reflectv = (-lightv).reflect(normalv);
                let reflect_dot_eye = reflectv.dot(eyev);
                if reflect_dot_eye <= 0. {
                    specular = BLACK;
                } else {
                    let factor = reflect_dot_eye.powf(self.shininess);
                    specular = *light.intensity() * self.specular * factor;
                };
            }
            (diffuse, specular)
        };
        ambient + diffuse + specular
    }
}

impl Default for Material {
    fn default() -> Self {
        Self::new()
    }
}

pub fn material() -> Material {
    Material::new()
}

impl PartialEq for Material {
    fn eq(&self, other: &Material) -> bool {
        self.color == other.color
            && float_eq_robust(self.ambient, other.ambient)
            && float_eq_robust(self.diffuse, other.diffuse)
            && float_eq_robust(self.specular, other.specular)
            && float_eq_robust(self.shininess, other.shininess)
    }
}

pub trait HasMaterial {
    fn material(&self) -> &Material;
    fn material_mut(&mut self) -> &mut Material;
    fn set_material(&mut self, m: Material);
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::sphere::Sphere;
    use crate::world::World;

    #[test]
    fn default_material() {
        let m = material();
        assert_eq!(m.color, WHITE);
        assert_eq!(m.ambient, 0.1);
        assert_eq!(m.diffuse, 0.9);
        assert_eq!(m.specular, 0.9);
        assert_eq!(m.shininess, 200.);
    }

    #[test]
    fn light_behind_eye() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let m = material();
        let position = ORIGIN;
        let eyev = vector!(0, 0, -1);
        let normalv = vector!(0, 0, -1);
        let light = point_light(point!(0, 0, -10), WHITE);
        let result = m.lighting(
            &Shape::Sphere(s),
            &light,
            &position,
            &eyev,
            &normalv,
            false,
            &w,
        );
        assert_eq!(result, color!(1.9, 1.9, 1.9));
    }

    #[test]
    fn light_45_below_eye() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let m = material();
        let position = ORIGIN;
        let eyev = vector!(0, SQRT_2 / 2., -SQRT_2 / 2.);
        let normalv = vector!(0, 0, -1);
        let light = point_light(point!(0, 0, -10), WHITE);
        let result = m.lighting(
            &Shape::Sphere(s),
            &light,
            &position,
            &eyev,
            &normalv,
            false,
            &w,
        );
        assert_eq!(result, WHITE);
    }

    #[test]
    fn light_above_eye() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let m = material();
        let position = ORIGIN;
        let eyev = vector!(0, 0, -1);
        let normalv = vector!(0, 0, -1);
        let light = point_light(point!(0, 10, -10), WHITE);
        let result = m.lighting(
            &Shape::Sphere(s),
            &light,
            &position,
            &eyev,
            &normalv,
            false,
            &w,
        );
        assert_eq!(result, color!(0.7364, 0.7364, 0.7364));
    }

    #[test]
    fn light_above_eye_reflected() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let m = material();
        let position = ORIGIN;
        let eyev = vector!(0, -SQRT_2 / 2., -SQRT_2 / 2.);
        let normalv = vector!(0, 0, -1);
        let light = point_light(point!(0, 10, -10), WHITE);
        let result = m.lighting(
            &Shape::Sphere(s),
            &light,
            &position,
            &eyev,
            &normalv,
            false,
            &w,
        );
        assert_eq!(result, color!(1.6364, 1.6364, 1.6364));
    }

    #[test]
    fn light_behind_surface() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let m = material();
        let position = ORIGIN;
        let eyev = vector!(0, 0, -1);
        let normalv = vector!(0, 0, -1);
        let light = point_light(point!(0, 0, 10), WHITE);
        let result = m.lighting(
            &Shape::Sphere(s),
            &light,
            &position,
            &eyev,
            &normalv,
            false,
            &w,
        );
        assert_eq!(result, color!(0.1, 0.1, 0.1));
    }

    #[test]
    fn surface_in_shadow() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let m = material();
        let eyev = vector!(0, 0, -1);
        let normalv = vector!(0, 0, -1);
        let light = point_light(point!(0, 0, -10), WHITE);
        let in_shadow = true;
        let result = m.lighting(
            &Shape::Sphere(s),
            &light,
            &ORIGIN,
            &eyev,
            &normalv,
            in_shadow,
            &w,
        );
        assert_eq!(result, color!(0.1, 0.1, 0.1));
    }

    #[test]
    fn lighting_with_pattern() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let mut m = material();
        m.pattern = Some(Rc::new(StripePattern::new(WHITE, BLACK)));
        m.ambient = 1.;
        m.diffuse = 0.;
        m.specular = 0.;
        let eyev = vector!(0, 0, -1);
        let normalv = vector!(0, 0, -1);
        let light = point_light(point!(0, 0, -10), WHITE);
        let s = Shape::Sphere(s);
        assert_eq!(
            m.lighting(&s, &light, &point!(0.9, 0, 0), &eyev, &normalv, false, &w),
            WHITE
        );
        assert_eq!(
            m.lighting(&s, &light, &point!(1.1, 0, 0), &eyev, &normalv, false, &w),
            BLACK
        );
    }

    #[test]
    fn reflectivity_for_default_material() {
        let m = material();
        assert_eq!(m.reflective, 0.);
    }

    #[test]
    fn transparency_and_refractivity_for_default_material() {
        let m = material();
        assert_eq!(m.transparency, 0.);
        assert_eq!(m.refractive_index, REFRACTIVE_VACUUM);
    }
}
