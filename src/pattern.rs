use crate::color::*;
use crate::matrix::*;
use crate::shape::{Shape, ShapeStore};
use crate::transform::*;
use crate::tuple::*;

pub trait Pattern: HasTransform {
    fn pattern_at(&self, p: &Point) -> Color;

    fn pattern_at_shape(&self, shape: &Shape, p: &Point, shape_store: &dyn ShapeStore) -> Color {
        let object_point = shape_store.world_to_object(shape, *p);
        let pattern_point = self.transform().inverse() * object_point;
        self.pattern_at(&pattern_point)
    }
}

#[derive(Clone, Debug)]
pub struct StripePattern {
    pub a: Color,
    pub b: Color,
    pub transform: Matrix4,
}

impl HasTransform for StripePattern {
    fn transform(&self) -> &Matrix4 {
        &self.transform
    }

    fn set_transform(&mut self, m: Matrix4) {
        self.transform = m;
    }
}

impl StripePattern {
    pub fn new(a: Color, b: Color) -> Self {
        StripePattern {
            a,
            b,
            transform: M4_ID,
        }
    }
}

impl Pattern for StripePattern {
    fn pattern_at(&self, p: &Point) -> Color {
        if (p.x.floor() as i64) % 2 == 0 {
            self.a
        } else {
            self.b
        }
    }
}

#[derive(Clone, Debug)]
pub struct GradientPattern {
    pub a: Color,
    pub b: Color,
    pub transform: Matrix4,
}

impl HasTransform for GradientPattern {
    fn transform(&self) -> &Matrix4 {
        &self.transform
    }

    fn set_transform(&mut self, m: Matrix4) {
        self.transform = m;
    }
}

impl GradientPattern {
    pub fn new(a: Color, b: Color) -> Self {
        GradientPattern {
            a,
            b,
            transform: M4_ID,
        }
    }
}

impl Pattern for GradientPattern {
    fn pattern_at(&self, p: &Point) -> Color {
        self.a + (self.b - self.a) * (p.x - p.x.floor())
    }
}

#[derive(Clone, Debug)]
pub struct RingPattern {
    pub a: Color,
    pub b: Color,
    pub transform: Matrix4,
}

impl HasTransform for RingPattern {
    fn transform(&self) -> &Matrix4 {
        &self.transform
    }

    fn set_transform(&mut self, m: Matrix4) {
        self.transform = m;
    }
}

impl RingPattern {
    pub fn new(a: Color, b: Color) -> Self {
        RingPattern {
            a,
            b,
            transform: M4_ID,
        }
    }
}

impl Pattern for RingPattern {
    fn pattern_at(&self, p: &Point) -> Color {
        if ((p.x * p.x + p.z * p.z).sqrt().floor() as u64) % 2 == 0 {
            self.a
        } else {
            self.b
        }
    }
}

#[derive(Clone, Debug)]
pub struct CheckersPattern {
    pub a: Color,
    pub b: Color,
    pub transform: Matrix4,
}

impl HasTransform for CheckersPattern {
    fn transform(&self) -> &Matrix4 {
        &self.transform
    }

    fn set_transform(&mut self, m: Matrix4) {
        self.transform = m;
    }
}

impl CheckersPattern {
    pub fn new(a: Color, b: Color) -> Self {
        CheckersPattern {
            a,
            b,
            transform: M4_ID,
        }
    }
}

impl Pattern for CheckersPattern {
    fn pattern_at(&self, p: &Point) -> Color {
        if ((p.x.floor() + p.y.floor() + p.z.floor()) as i64) % 2 == 0 {
            self.a
        } else {
            self.b
        }
    }
}

pub struct BlendedPattern {
    pub a: Box<dyn Pattern>,
    pub b: Box<dyn Pattern>,
    pub transform: Matrix4,
}

impl HasTransform for BlendedPattern {
    fn transform(&self) -> &Matrix4 {
        &self.transform
    }

    fn set_transform(&mut self, m: Matrix4) {
        self.transform = m;
    }
}

impl BlendedPattern {
    pub fn new(a: impl Pattern + 'static, b: impl Pattern + 'static) -> Self {
        BlendedPattern {
            a: Box::new(a),
            b: Box::new(b),
            transform: M4_ID,
        }
    }
}

impl Pattern for BlendedPattern {
    fn pattern_at_shape(&self, shape: &Shape, p: &Point, shape_store: &dyn ShapeStore) -> Color {
        let a = self.a.pattern_at_shape(shape, p, shape_store);
        let b = self.b.pattern_at_shape(shape, p, shape_store);
        a * 0.5 + b * 0.5
    }

    fn pattern_at(&self, _: &Point) -> Color {
        panic!("BlendedPattern::pattern_at should never be used directly")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn background() {
        assert_eq!(BLACK, color!(0, 0, 0));
        assert_eq!(WHITE, color!(1, 1, 1));
    }

    #[test]
    fn stripe_pattern() {
        let pattern = StripePattern::new(WHITE, BLACK);
        assert_eq!(pattern.a, WHITE);
        assert_eq!(pattern.b, BLACK);
    }

    #[test]
    fn stripe_pattern_is_constant_in_y() {
        let pattern = StripePattern::new(WHITE, BLACK);
        assert_eq!(pattern.pattern_at(&point!(0, 0, 0)), WHITE);
        assert_eq!(pattern.pattern_at(&point!(0, 1, 0)), WHITE);
        assert_eq!(pattern.pattern_at(&point!(0, 2, 0)), WHITE);
    }

    #[test]
    fn stripe_pattern_is_constant_in_z() {
        let pattern = StripePattern::new(WHITE, BLACK);
        assert_eq!(pattern.pattern_at(&point!(0, 0, 0)), WHITE);
        assert_eq!(pattern.pattern_at(&point!(0, 0, 1)), WHITE);
        assert_eq!(pattern.pattern_at(&point!(0, 0, 2)), WHITE);
    }

    #[test]
    fn stripe_pattern_alternates_in_x() {
        let pattern = StripePattern::new(WHITE, BLACK);
        assert_eq!(pattern.pattern_at(&point!(0, 0, 0)), WHITE);
        assert_eq!(pattern.pattern_at(&point!(0.9, 0, 0)), WHITE);
        assert_eq!(pattern.pattern_at(&point!(1, 0, 0)), BLACK);
        assert_eq!(pattern.pattern_at(&point!(-0.1, 0, 0)), BLACK);
        assert_eq!(pattern.pattern_at(&point!(-1, 0, 0)), BLACK);
        assert_eq!(pattern.pattern_at(&point!(-1.1, 0, 0)), WHITE);
    }
}

#[cfg(test)]
pub mod abstract_pattern_tests {
    use super::*;
    use crate::sphere::Sphere;
    use crate::world::World;

    pub struct TestPattern {
        transform: Matrix4,
    }

    impl HasTransform for TestPattern {
        fn transform(&self) -> &Matrix4 {
            &self.transform
        }

        fn set_transform(&mut self, m: Matrix4) {
            self.transform = m;
        }
    }

    impl TestPattern {
        pub fn new() -> Self {
            TestPattern { transform: M4_ID }
        }
    }

    impl Pattern for TestPattern {
        fn pattern_at(&self, p: &Point) -> Color {
            color!(p.x, p.y, p.z)
        }
    }

    #[test]
    fn default_pattern_transformation() {
        let pattern = TestPattern::new();
        assert_eq!(*pattern.transform(), M4_ID);
    }

    #[test]
    fn assign_a_transform() {
        let mut pattern = TestPattern::new();
        pattern.set_transform(translation(1., 2., 3.));
        assert_eq!(*pattern.transform(), translation(1., 2., 3.));
    }

    #[test]
    fn pattern_with_object_transformation() {
        let mut w = World::new();
        let mut s = Sphere::new(&mut w);
        s.set_transform(scaling(2., 2., 2.));
        let pattern = TestPattern::new();
        assert_eq!(
            pattern.pattern_at_shape(&Shape::Sphere(s), &point!(2, 3, 4), &w),
            color!(1, 1.5, 2)
        );
    }

    #[test]
    fn pattern_with_pattern_transformation() {
        let mut w = World::new();
        let s = Sphere::new(&mut w);
        let mut pattern = TestPattern::new();
        pattern.set_transform(scaling(2., 2., 2.));
        assert_eq!(
            pattern.pattern_at_shape(&Shape::Sphere(s), &point!(2, 3, 4), &w),
            color!(1, 1.5, 2)
        );
    }

    #[test]
    fn pattern_with_both_object_and_pattern_transformations() {
        let mut w = World::new();
        let mut s = Sphere::new(&mut w);
        s.set_transform(scaling(2., 2., 2.));
        let mut pattern = TestPattern::new();
        pattern.set_transform(translation(0.5, 1., 1.5));
        assert_eq!(
            pattern.pattern_at_shape(&Shape::Sphere(s), &point!(2.5, 3, 3.5), &w),
            color!(0.75, 0.5, 0.25)
        );
    }

    #[test]
    fn gradient_pattern_linearly_interpolates() {
        let pattern = GradientPattern::new(WHITE, BLACK);
        assert_eq!(pattern.pattern_at(&point!(0, 0, 0)), WHITE);
        assert_eq!(
            pattern.pattern_at(&point!(0.25, 0, 0)),
            color!(0.75, 0.75, 0.75)
        );
        assert_eq!(
            pattern.pattern_at(&point!(0.5, 0, 0)),
            color!(0.5, 0.5, 0.5)
        );
        assert_eq!(
            pattern.pattern_at(&point!(0.75, 0, 0)),
            color!(0.25, 0.25, 0.25)
        );
    }

    #[test]
    fn ring_pattern_extends_in_both_x_and_z() {
        let pattern = RingPattern::new(WHITE, BLACK);
        assert_eq!(pattern.pattern_at(&point!(0, 0, 0)), WHITE);
        assert_eq!(pattern.pattern_at(&point!(1, 0, 0)), BLACK);
        assert_eq!(pattern.pattern_at(&point!(0, 0, 1)), BLACK);
        assert_eq!(pattern.pattern_at(&point!(0.708, 0, 0.708)), BLACK);
    }

    #[test]
    fn checkers_pattern_repeats_in_x() {
        let pattern = CheckersPattern::new(WHITE, BLACK);
        assert_eq!(pattern.pattern_at(&point!(0, 0, 0)), WHITE);
        assert_eq!(pattern.pattern_at(&point!(0.99, 0, 0)), WHITE);
        assert_eq!(pattern.pattern_at(&point!(1.01, 0, 0)), BLACK);
    }

    #[test]
    fn checkers_pattern_repeats_in_y() {
        let pattern = CheckersPattern::new(WHITE, BLACK);
        assert_eq!(pattern.pattern_at(&point!(0, 0, 0)), WHITE);
        assert_eq!(pattern.pattern_at(&point!(0, 0.99, 0)), WHITE);
        assert_eq!(pattern.pattern_at(&point!(0, 1.01, 0)), BLACK);
    }

    #[test]
    fn checkers_pattern_repeats_in_z() {
        let pattern = CheckersPattern::new(WHITE, BLACK);
        assert_eq!(pattern.pattern_at(&point!(0, 0, 0)), WHITE);
        assert_eq!(pattern.pattern_at(&point!(0, 0, 0.99)), WHITE);
        assert_eq!(pattern.pattern_at(&point!(0, 0, 1.01)), BLACK);
    }
}
