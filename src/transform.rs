use crate::matrix::*;
use crate::tuple::*;

pub use std::f64::consts::PI;

pub fn translation(x: f64, y: f64, z: f64) -> Matrix4 {
    mat4([
        [1., 0., 0., x],
        [0., 1., 0., y],
        [0., 0., 1., z],
        [0., 0., 0., 1.],
    ])
}

pub fn scaling(x: f64, y: f64, z: f64) -> Matrix4 {
    mat4([
        [x, 0., 0., 0.],
        [0., y, 0., 0.],
        [0., 0., z, 0.],
        [0., 0., 0., 1.],
    ])
}

pub fn rotation_x(r: f64) -> Matrix4 {
    mat4([
        [1., 0., 0., 0.],
        [0., r.cos(), -r.sin(), 0.],
        [0., r.sin(), r.cos(), 0.],
        [0., 0., 0., 1.0],
    ])
}

pub fn rotation_y(r: f64) -> Matrix4 {
    mat4([
        [r.cos(), 0., r.sin(), 0.],
        [0., 1., 0., 0.],
        [-r.sin(), 0., r.cos(), 0.],
        [0., 0., 0., 1.0],
    ])
}

pub fn rotation_z(r: f64) -> Matrix4 {
    mat4([
        [r.cos(), -r.sin(), 0., 0.],
        [r.sin(), r.cos(), 0., 0.],
        [0., 0., 1., 0.],
        [0., 0., 0., 1.0],
    ])
}

pub fn shearing(x_y: f64, x_z: f64, y_x: f64, y_z: f64, z_x: f64, z_y: f64) -> Matrix4 {
    mat4([
        [1., x_y, x_z, 0.],
        [y_x, 1., y_z, 0.],
        [z_x, z_y, 1., 0.],
        [0., 0., 0., 1.],
    ])
}

pub fn view_transform(from: Point, to: Point, up: Vector) -> Matrix4 {
    let forward = (to - from).normalize();
    let left = forward.cross(up.normalize());
    let true_up = left.cross(forward);
    let orientation = mat4([
        [left.x, left.y, left.z, 0.],
        [true_up.x, true_up.y, true_up.z, 0.],
        [-forward.x, -forward.y, -forward.z, 0.],
        [0., 0., 0., 1.],
    ]);
    orientation * translation(-from.x, -from.y, -from.z)
}

pub trait HasTransform {
    fn transform(&self) -> &Matrix4;

    fn set_transform(&mut self, m: Matrix4);
}

pub trait Transformable {
    fn translate(&self, x: f64, y: f64, z: f64) -> Self;
    fn scale(&self, x: f64, y: f64, z: f64) -> Self;
    fn rotate_x(&self, r: f64) -> Self;
    fn rotate_y(&self, r: f64) -> Self;
    fn rotate_z(&self, r: f64) -> Self;
    fn shear(&self, x_y: f64, x_z: f64, y_x: f64, y_z: f64, z_x: f64, z_y: f64) -> Self;
}

impl Transformable for Tuple {
    fn translate(&self, x: f64, y: f64, z: f64) -> Self {
        translation(x, y, z) * *self
    }

    fn scale(&self, x: f64, y: f64, z: f64) -> Self {
        scaling(x, y, z) * *self
    }

    fn rotate_x(&self, r: f64) -> Self {
        rotation_x(r) * *self
    }

    fn rotate_y(&self, r: f64) -> Self {
        rotation_y(r) * *self
    }

    fn rotate_z(&self, r: f64) -> Self {
        rotation_z(r) * *self
    }

    fn shear(&self, x_y: f64, x_z: f64, y_x: f64, y_z: f64, z_x: f64, z_y: f64) -> Self {
        shearing(x_y, x_z, y_x, y_z, z_x, z_y) * *self
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn translation1() {
        let t = translation(5., -3., 2.);
        let p = point!(-3, 4, 5);
        assert_eq!(t * p, point!(2, 1, 7));
    }

    #[test]
    fn translation2() {
        let t = translation(5., -3., 2.);
        let inv = t.inverse();
        let p = point!(-3, 4, 5);
        assert_eq!(inv * p, point!(-8, 7, 3));
    }

    #[test]
    fn translation3() {
        let t = translation(5., -3., 2.);
        let v = vector!(-3, 4, 5);
        assert_eq!(t * v, v);
    }

    #[test]
    fn scaling1() {
        let t = scaling(2., 3., 4.);
        let p = point!(-4, 6, 8);
        assert_eq!(t * p, point!(-8, 18, 32))
    }

    #[test]
    fn scaling2() {
        let t = scaling(2., 3., 4.);
        let v = vector!(-4, 6, 8);
        assert_eq!(t * v, vector!(-8, 18, 32))
    }

    #[test]
    fn scaling3() {
        let t = scaling(2., 3., 4.);
        let inv = t.inverse();
        let v = vector!(-4, 6, 8);
        assert_eq!(inv * v, vector!(-2, 2, 2))
    }

    #[test]
    fn reflection() {
        let t = scaling(-1., 1., 1.);
        let p = point!(2, 3, 4);
        assert_eq!(t * p, point!(-2, 3, 4))
    }

    #[test]
    fn rotation1() {
        let p = point!(0, 1, 0);
        let half_quarter = rotation_x(PI / 4.);
        let full_quarter = rotation_x(PI / 2.);
        assert_eq!(half_quarter * p, point!(0, SQRT_2 / 2., SQRT_2 / 2.));
        assert_eq!(full_quarter * p, point!(0, 0, 1));
    }

    #[test]
    fn rotation2() {
        let p = point!(0, 1, 0);
        let half_quarter = rotation_x(PI / 4.);
        let inv = half_quarter.inverse();
        assert_eq!(inv * p, point!(0, SQRT_2 / 2., -SQRT_2 / 2.));
    }

    #[test]
    fn rotation3() {
        let p = point!(0, 0, 1);
        let half_quarter = rotation_y(PI / 4.);
        let full_quarter = rotation_y(PI / 2.);
        assert_eq!(half_quarter * p, point!(SQRT_2 / 2., 0, SQRT_2 / 2.));
        assert_eq!(full_quarter * p, point!(1, 0, 0));
    }

    #[test]
    fn rotation4() {
        let p = point!(0, 1, 0);
        let half_quarter = rotation_z(PI / 4.);
        let full_quarter = rotation_z(PI / 2.);
        assert_eq!(half_quarter * p, point!(-SQRT_2 / 2., SQRT_2 / 2., 0));
        assert_eq!(full_quarter * p, point!(-1, 0, 0));
    }

    #[test]
    fn shearing1() {
        let t = shearing(0., 1., 0., 0., 0., 0.);
        let p = point!(2, 3, 4);
        assert_eq!(t * p, point!(6, 3, 4));
    }

    #[test]
    fn shearing2() {
        let t = shearing(0., 0., 1., 0., 0., 0.);
        let p = point!(2, 3, 4);
        assert_eq!(t * p, point!(2, 5, 4));
    }

    #[test]
    fn shearing3() {
        let t = shearing(0., 0., 0., 1., 0., 0.);
        let p = point!(2, 3, 4);
        assert_eq!(t * p, point!(2, 7, 4));
    }

    #[test]
    fn shearing4() {
        let t = shearing(0., 0., 0., 0., 1., 0.);
        let p = point!(2, 3, 4);
        assert_eq!(t * p, point!(2, 3, 6));
    }

    #[test]
    fn shearing5() {
        let t = shearing(0., 0., 0., 0., 0., 1.);
        let p = point!(2, 3, 4);
        assert_eq!(t * p, point!(2, 3, 7));
    }

    #[test]
    fn chain1() {
        let p = point!(1, 0, 1);
        let a = rotation_x(PI / 2.);
        let b = scaling(5., 5., 5.);
        let c = translation(10., 5., 7.);
        let p2 = a * p;
        assert_eq!(p2, point!(1, -1, 0));
        let p3 = b * p2;
        assert_eq!(p3, point!(5, -5, 0));
        let p4 = c * p3;
        assert_eq!(p4, point!(15, 0, 7));
    }

    #[test]
    fn chain2() {
        let p = point!(1, 0, 1);
        assert_eq!(
            p.rotate_x(PI / 2.).scale(5., 5., 5.).translate(10., 5., 7.),
            point!(15, 0, 7)
        );
    }

    #[test]
    fn default_view_transform() {
        let from = point!(0, 0, 0);
        let to = point!(0, 0, -1);
        let up = vector!(0, 1, 0);
        let t = view_transform(from, to, up);
        assert_eq!(t, M4_ID);
    }

    #[test]
    fn view_transform_look_behind() {
        let from = point!(0, 0, 0);
        let to = point!(0, 0, 1);
        let up = vector!(0, 1, 0);
        let t = view_transform(from, to, up);
        assert_eq!(t, scaling(-1., 1., -1.));
    }

    #[test]
    fn view_transform_move_eye() {
        let from = point!(0, 0, 8);
        let to = point!(0, 0, 0);
        let up = vector!(0, 1, 0);
        let t = view_transform(from, to, up);
        assert_eq!(t, translation(0., 0., -8.));
    }

    #[test]
    fn view_transform_arbitrary() {
        let from = point!(1, 3, 2);
        let to = point!(4, -2, 8);
        let up = vector!(1, 1, 0);
        let t = view_transform(from, to, up);
        assert_eq!(
            t,
            mat4([
                [-0.50709, 0.50709, 0.67612, -2.36643],
                [0.76772, 0.60609, 0.12122, -2.82843],
                [-0.35857, 0.59761, -0.71714, 0.0],
                [0.0, 0.0, 0.0, 1.0],
            ])
        );
    }
}
