#[macro_export]
macro_rules! shape_boilerplate {
    ($struct_name:ident) => {
        impl crate::entity::Entity for $struct_name {
            fn eid(&self) -> crate::eid::Eid {
                self.eid
            }
        }

        impl crate::entity::Entity for &$struct_name {
            fn eid(&self) -> crate::eid::Eid {
                self.eid
            }
        }

        impl crate::material::HasMaterial for $struct_name {
            fn material(&self) -> &crate::material::Material {
                &self.material
            }

            fn material_mut(&mut self) -> &mut crate::material::Material {
                &mut self.material
            }

            fn set_material(&mut self, m: crate::material::Material) {
                self.material = m;
            }
        }

        impl crate::transform::HasTransform for $struct_name {
            fn transform(&self) -> &crate::matrix::Matrix4 {
                &self.transform
            }

            fn set_transform(&mut self, m: crate::matrix::Matrix4) {
                self.transform = m;
            }
        }

        impl crate::parent::HasParent for $struct_name {
            fn parent(&self) -> Option<crate::eid::Eid> {
                self.parent.clone()
            }

            fn set_parent(&mut self, parent: &crate::eid::Eid) {
                self.parent = Some(parent.clone());
            }
        }

        // Rust won't let us write a blanket implementation for `PartialEq` for
        // all `E: Entity` because `PartialEq` wasn't defined in this crate, so
        // we use a macro.
        impl PartialEq for $struct_name {
            fn eq(&self, other: &Self) -> bool {
                self.eid == other.eid
            }
        }
    };
}
