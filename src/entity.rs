use crate::eid::*;

pub trait Entity {
    fn eid(&self) -> Eid;
}
