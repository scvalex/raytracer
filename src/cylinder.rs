use crate::contains::*;
use crate::eid::*;
use crate::intersect::*;
use crate::material::*;
use crate::matrix::*;
use crate::normal::*;
use crate::ray::*;
use crate::robust_eq::*;
use crate::shape::ShapeStore;
use crate::tuple::*;

#[derive(Clone, Debug)]
pub struct Cylinder {
    eid: Eid,
    transform: Matrix4,
    material: Material,
    parent: Option<Eid>,
    pub minimum: f64,
    pub maximum: f64,
    pub closed: bool,
}

shape_boilerplate!(Cylinder);

impl HasContains for Cylinder {
    fn contains(&self, eid: &Eid, _: &dyn ShapeStore) -> bool {
        self.eid == *eid
    }
}

impl Cylinder {
    pub fn new(w: &mut dyn EidAllocator) -> Self {
        Cylinder {
            eid: w.new_eid(),
            transform: M4_ID,
            material: Material::default(),
            parent: None,
            minimum: std::f64::NEG_INFINITY,
            maximum: std::f64::INFINITY,
            closed: false,
        }
    }

    fn intersect_caps(&self, r: &Ray, xs: &mut Vec<Intersection>) {
        if self.closed && !float_eq_robust(r.direction.y, 0.) {
            let t = (self.minimum - r.origin.y) / r.direction.y;
            if check_cap(r, t) {
                xs.push(intersection(t, self.eid));
            }
            let t = (self.maximum - r.origin.y) / r.direction.y;
            if check_cap(r, t) {
                xs.push(intersection(t, self.eid))
            }
        }
    }
}

fn check_cap(r: &Ray, t: f64) -> bool {
    let x = r.origin.x + t * r.direction.x;
    let z = r.origin.z + t * r.direction.z;
    (x * x + z * z) <= 1.
}

impl HasIntersect for Cylinder {
    fn local_intersect(&self, r: &Ray, _: &dyn ShapeStore) -> Vec<Intersection> {
        let a = r.direction.x * r.direction.x + r.direction.z * r.direction.z;
        if float_eq_robust(a, 0.) {
            let mut xs = vec![];
            self.intersect_caps(r, &mut xs);
            xs
        } else {
            let b = 2. * r.origin.x * r.direction.x + 2. * r.origin.z * r.direction.z;
            let c = r.origin.x * r.origin.x + r.origin.z * r.origin.z - 1.;
            let disc = b * b - 4. * a * c;
            if disc < 0. {
                vec![]
            } else {
                let mut t0 = (-b - disc.sqrt()) / (2. * a);
                let mut t1 = (-b + disc.sqrt()) / (2. * a);
                if t0 > t1 {
                    std::mem::swap(&mut t0, &mut t1);
                }
                let mut xs = vec![];
                let y0 = r.origin.y + t0 * r.direction.y;
                if self.minimum < y0 && y0 < self.maximum {
                    xs.push(intersection(t0, self.eid))
                }
                let y1 = r.origin.y + t1 * r.direction.y;
                if self.minimum < y1 && y1 < self.maximum {
                    xs.push(intersection(t1, self.eid))
                }
                self.intersect_caps(r, &mut xs);
                xs
            }
        }
    }
}

impl HasNormal for Cylinder {
    #[allow(clippy::float_cmp)]
    fn local_normal_at(&self, p: &Point, _: &Intersection, _: &dyn ShapeStore) -> Vector {
        let dist = p.x * p.x + p.z * p.z;
        if dist < 1. && p.y >= self.maximum - 1e-5 {
            vector!(0, 1, 0)
        } else if dist < 1. && p.y <= self.minimum + 1e-5 {
            vector!(0, -1, 0)
        } else {
            vector!(p.x, 0, p.z)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::world::World;

    #[test]
    fn ray_misses_cylinder() {
        let mut w = World::new();
        let cyl = Cylinder::new(&mut w);
        let cases = [
            (point!(1, 0, 0), vector!(0, 1, 0)),
            (point!(0, 0, 0), vector!(0, 1, 0)),
            (point!(0, 0, -5), vector!(1, 1, 1)),
        ];
        for (i, (origin, direction)) in cases.iter().enumerate() {
            let direction = direction.normalize();
            let r = ray(*origin, direction);
            let xs = cyl.local_intersect(&r, &w);
            assert_eq!(xs.len(), 0, "Test #{}", i);
        }
    }

    #[test]
    fn ray_strikes_cylinder() {
        let mut w = World::new();
        let cyl = Cylinder::new(&mut w);
        let cases = [
            (point!(1, 0, -5), vector!(0, 0, 1), 5., 5.),
            (point!(0, 0, -5), vector!(0, 0, 1), 4., 6.),
            (point!(0.5, 0, -5), vector!(0.1, 1, 1), 6.80798, 7.08872),
        ];
        for (i, (origin, direction, t0, t1)) in cases.iter().enumerate() {
            let direction = direction.normalize();
            let r = ray(*origin, direction);
            let xs = cyl.local_intersect(&r, &w);
            assert_eq!(xs.len(), 2, "Test #{}", i);
            assert!(float_eq_robust(xs[0].t, *t0), "Test #{} t0", i);
            assert!(float_eq_robust(xs[1].t, *t1), "Test #{} t1", i);
        }
    }

    #[test]
    fn normal_vector_on_cylinder() {
        let mut w = World::new();
        let cyl = Cylinder::new(&mut w);
        let cases = [
            (point!(1, 0, 0), vector!(1, 0, 0)),
            (point!(0, 5, -1), vector!(0, 0, -1)),
            (point!(0, -2, 1), vector!(0, 0, 1)),
            (point!(-1, 1, 0), vector!(-1, 0, 0)),
        ];
        for (i, (point, normal)) in cases.iter().enumerate() {
            let n = cyl.local_normal_at(point, &intersection(1., cyl.eid), &w);
            assert_eq!(n, *normal, "Test #{}", i);
        }
    }

    #[test]
    fn default_minimum_and_maximum() {
        let cyl = Cylinder::new(&mut World::new());
        assert_eq!(cyl.minimum, std::f64::NEG_INFINITY);
        assert_eq!(cyl.maximum, std::f64::INFINITY);
    }

    #[test]
    fn intersecting_a_constrained_cylinder() {
        let mut w = World::new();
        let mut cyl = Cylinder::new(&mut w);
        cyl.minimum = 1.;
        cyl.maximum = 2.;
        let cases = [
            (point!(0, 1.5, 0), vector!(0.1, 1, 0), 0),
            (point!(0, 3, -5), vector!(0, 0, 1), 0),
            (point!(0, 0, -5), vector!(0, 0, 1), 0),
            (point!(0, 2, -5), vector!(0, 0, 1), 0),
            (point!(0, 1, -5), vector!(0, 0, 1), 0),
            (point!(0, 1.5, -2), vector!(0, 0, 1), 2),
        ];
        for (i, (point, direction, count)) in cases.iter().enumerate() {
            let direction = direction.normalize();
            let r = ray(*point, direction);
            let xs = cyl.local_intersect(&r, &w);
            assert_eq!(xs.len(), *count, "Test #{}", i);
        }
    }

    #[test]
    fn cylinders_are_open_by_default() {
        let cyl = Cylinder::new(&mut World::new());
        assert_eq!(cyl.closed, false);
    }

    #[test]
    fn intersect_caps_of_closed_cylinder() {
        let mut w = World::new();
        let mut cyl = Cylinder::new(&mut w);
        cyl.minimum = 1.;
        cyl.maximum = 2.;
        cyl.closed = true;
        let cases = [
            (point!(0, 3, 0), vector!(0, -1, 0), 2),
            (point!(0, 3, -2), vector!(0, -1, 2), 2),
            (point!(0, 4, -2), vector!(0, -1, 1), 2),
            (point!(0, 0, -2), vector!(0, 1, 2), 2),
            (point!(0, -1, -2), vector!(0, 1, 1), 2),
        ];
        for (i, (point, direction, count)) in cases.iter().enumerate() {
            let direction = direction.normalize();
            let r = ray(*point, direction);
            let xs = cyl.local_intersect(&r, &w);
            assert_eq!(xs.len(), *count, "Test #{}", i);
        }
    }

    #[test]
    fn nromal_on_a_cylinder_cap() {
        let mut w = World::new();
        let mut cyl = Cylinder::new(&mut w);
        cyl.minimum = 1.;
        cyl.maximum = 2.;
        cyl.closed = true;
        let cases = [
            (point!(0, 1, 0), vector!(0, -1, 0)),
            (point!(0.5, 1, 0), vector!(0, -1, 0)),
            (point!(0, 1, 0.5), vector!(0, -1, 0)),
            (point!(0, 2, 0), vector!(0, 1, 0)),
            (point!(0.5, 2, 0), vector!(0, 1, 0)),
            (point!(0, 2, 0.5), vector!(0, 1, 0)),
        ];
        for (i, (point, normal)) in cases.iter().enumerate() {
            let n = cyl.local_normal_at(
                point,
                &intersection((*point - ORIGIN).magnitude(), cyl.eid),
                &w,
            );
            assert_eq!(n, *normal, "Test #{}", i);
        }
    }
}
